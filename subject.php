<?php
    include('header.php');
    include('side-bar.php');
?>
    <div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Subject Information</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Subject</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">New Subject</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Subject Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="submit-form" method="post" class="form-horizontal">
										<div class="form-body">
										
                                            <div class="form-group row">
												<label class="control-label col-md-3" for="txtsubject">Subject
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtsubject" id="txtsubject" placeholder="Enter Subject Name" class="form-control input-height" /> </div>
											</div>
                                            <div class="form-group row">
												<label class="control-label col-md-3" for="multiple">Medium
													<span class="required"> * </span>
												</label>
									
												<div class="col-md-5 ">
												<div class="checkbox checkbox-aqua">
													<input id="txtmedium" name="txtmedium" type="checkbox" checked="checked" value="Tamil">
													<label for="checkboxbg4">
															Tamil
													</label>
											
													<input id="txtmedium" type="checkbox" name="txtmedium" value="English">
													<label for="checkboxbg4">
															English
													</label>
                         </div>
												</div>
																						</div>
																				
                                            <div class="form-actions">
												<div class="row">
													<div class="offset-md-3 col-md-9">
														<button type="submit" name="submit-subject" id="submit-subject" class="btn btn-info m-r-20">Submit</button>
														<button type="button" class="btn btn-default">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>

	
<!-- <div class="container">
  

 
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <div class="modal-body">
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
</div> -->
          
<?php
include('footer.php');
?>

	<script>

// for save the data
	function save(){
		var result = "subject";
	
	
		var medium =$('#multiple').serializeArray();
		var txtsubject =$('#txtsubject').val();
		var selectedLanguage = new Array();
		$('input[name="txtmedium"]:checked').each(function() {
		selectedLanguage.push(this.value);
		});
		var data ="&result="+result+"&selectedLanguage="+selectedLanguage+"&txtsubject="+txtsubject;
	
		$.ajax({

		method:'POST',
		url:"load/save.php",
		data:data,
		dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			if(data=="Records inserted successfully.")
			{
				$.toast({
						heading: 'Subject Successfully Added',
						text: 'Data Added Successfully',
						position: 'top-right',
						loaderBg:'#ff6849',
						icon: 'success',
						hideAfter: 3500, 
						stack: 6
				});
			}
			else if(data=="Subject Already Added.")
			{
				$.toast({
					heading: 'Subject Already Added.',
					text: 'Record Already Added.',
					position: 'top-right',
					loaderBg:'#ff6849',
					icon: 'error',
					hideAfter: 3500
					
				});
			}
		

		$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');



		})
		.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 

		});
	}
	//for required
	function required(){
		$.toast({
			heading: 'Please Fill The All Details.',
			text: 'All fileds are must.',
			position: 'top-right',
			loaderBg:'#ff6849',
			icon: 'error',
			hideAfter: 3500
					
		});
	}


	$(document).ready(function(){
			
		$('input[type=text]').css('text-transform','capitalize');
	
		$( "form" ).on( "submit", function( event ) {
				event.preventDefault();
				//required validation
			
		
					//console.log(medium);
					if($('#txtsubject').val() =="" || $('#txtmedium').val()=="" )
					{
						required();
					}
					else
					{
						save();
					}
			

	})
			

	})
			
</script>  