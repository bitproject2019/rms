<?php
 $my_url =  $_SERVER['REQUEST_URI']; 
$active = substr($my_url, strrpos($my_url, '/' )+1);	
$active2 = substr($my_url, strrpos($my_url, '?' )+1);	
$username = $_SESSION['fullname'];
$usertype = $_SESSION['user_type'];
// if (strpos($active, '?') !== false) {
//     $active = substr($active, strrpos($active, '?' )+1);
//  }
 
 ?>
<div class="page-container">
    <!-- start sidebar menu -->
    <div class="sidebar-container">
        <div class="sidemenu-container navbar-collapse collapse fixed-menu">
            <div id="remove-scroll" class="left-sidemenu">
                <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true"
                 data-slide-speed="200" style="padding-top: 20px">
                    <li class="sidebar-toggler-wrapper hide">
                        <div class="sidebar-toggler">
                            <span></span>
                        </div>
                    </li>
                    <li class="sidebar-user-panel">
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="assets/img/Vembadi.png" class="img-circle user-img-circle" alt="User Image" />
                            </div>
                            <div class="pull-left info">
                                <?php echo $username; ?>
                                
                            </div>
                        </div>
                    </li>
                    <li class="<?php echo ( $active == 'index' || $active == '' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="index" class="nav-link nav-toggle">
                            <i class="material-icons">dashboard</i>
                            <span class="title">Dashboard</span>
                            
                        </a>
                       
                    </li>
                    
                    <li class="<?php echo ( $active == 'result' || $active == 'results' || $active =='results?Bio'||$active =='results?Maths'||$active =='results?Arts'||$active =='results?Commerce'||$active =='results?Eng-Technology'||$active =='results?Bio-Technology'||$active =='results?Other'? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle"> <i class="material-icons">assignment_turned_in</i>
                            <span class="title">Result</span>
                            <span class="<?php echo ( $active == 'result' || $active == 'results' ? 'arrow open' : 'arrow' );?>"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'result' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="result" class="nav-link "> 
                                <span class="title">New Result</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active =='results?Bio'||$active =='results?Maths'||$active =='results?Arts'||$active =='results?Commerce'||$active =='results?Eng-Technology'||$active =='results?Bio-Technology'||$active =='results?Other' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="results?Bio" class="nav-link "> <span class="title">All Result</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li id="grade" class="<?php echo ( $active == 'grade' || $active == 'grades' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle"><i class="material-icons">font_download</i>
                            <span class="title">Division</span>
                            <span class="<?php echo ( $active == 'grade' || $active == 'grades' ? 'arrow open' : 'arrow' );?>"></span></a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'grade' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="grade" class="nav-link "> <span class="title">Add Division</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'grades' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="grades" class="nav-link "> <span class="title">All Division</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?php echo ( $active == 'subject' || $active == 'subjects' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">school</i>
                        <span class="title">Subject</span> 
                        <span class="<?php echo ( $active == 'subject' || $active == 'subjects' ? 'arrow open' : 'arrow' );?>"></span></a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'subject' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="subject" class="nav-link ">
                                <span class="title">New Subject</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'subjects' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="subjects" class="nav-link ">
                                <span class="title">All Subject</span>
                                </a>
                            </li>
                       </ul>
                    </li>
                    <li class="<?php echo ( $active == 'stream' || $active == 'streams' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle"> <i class="material-icons">local_library</i>
                            <span class="title">Stream</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'stream' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="stream" class="nav-link ">
                                <span class="title">New Stream</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'streams' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="streams" class="nav-link ">
                                <span class="title">All Stream</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?php echo ( $active == 'results-report' || $active == 'results-report-third'|| $active == 'results-report-second' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle"> <i class="material-icons">receipt</i>
                            <span class="title">Results Analysis</span> 
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'results-report' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="results-report" class="nav-link ">
                                <span class="title">First Attempt</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'results-report-second' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="results-report-second" class="nav-link ">
                                <span class="title">Second Attempt</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'results-report-third' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="results-report-third" class="nav-link ">
                                <span class="title">Third Attempt</span>
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <li class="<?php echo ( $active == 'subjects-report' || $active == 'subject-results-third'|| $active == 'subject-results-second' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle"> <i class="material-icons">folder</i>
                            <span class="title">Subjects Analysis</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'subjects-report' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="subjects-report" class="nav-link "> <span class="title">First Attempt</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'subject-results-second' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="subject-results-second" class="nav-link "> <span class="title">Second Attempt</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'subject-results-third' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="subject-results-third" class="nav-link "> <span class="title">Third Attempt</span>
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <li class="<?php echo ( $active == 'report-pass' || $active == 'report-pass-third'|| $active == 'report-pass-second'|| $active == 'report-pass-total' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle"> <i class="material-icons">star_half</i>
                            <span class="title">Pass Percentage</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'report-pass' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="report-pass" class="nav-link "> <span class="title">First Attempt</span>
                              	
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'report-pass-second' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="report-pass-second" class="nav-link "> <span class="title">Second Attempt</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'report-pass-third' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="report-pass-third" class="nav-link "> <span class="title">Third Attempt</span>
                                </a>
                            </li>
                            <li class="<?php echo ( $active == 'report-pass-total' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="report-pass-total" class="nav-link "> <span class="title">Total Pass Percentage</span>
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <li class="<?php echo ( $active == 'results-stream-chart' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle">
                            <i class="material-icons">assessment</i>
                            <span class="title">Stream Results</span>
                            <span class="arrow"></span>
                           
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'results-stream-chart' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="results-stream-chart" class="nav-link ">
                                    <span class="title">Stream Results
                               </span>
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <li class="<?php echo ( $active == 'change-password' ? 'nav-item active' : 'nav-item' );?>">
                        <a href="#" class="nav-link nav-toggle">
                            <i class="material-icons f-left">settings</i>
                            <span class="title">Change Password</span>
                            <span class="arrow"></span>
                           
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php echo ( $active == 'change-password' ? 'nav-item active' : 'nav-item' );?>">
                                <a href="change-password" class="nav-link ">
                                    <span class="title">Change Password
                               </span>
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <?php

                    if($usertype=="Admin")
                    {

                    ?>
                        <li class="<?php echo ( $active == 'login-verification' ? 'nav-item active' : 'nav-item' );?>">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="material-icons f-left">account_circle</i>
                                <span class="title">User Verification</span>
                                <span class="arrow"></span>
                            
                            </a>
                            <ul class="sub-menu">
                                <li class="<?php echo ( $active == 'login-verification' ? 'nav-item active' : 'nav-item' );?>">
                                    <a href="login-verification" class="nav-link ">
                                        <span class="title">User Verification
                                </span>
                                    </a>
                                </li>
                            
                            </ul>
                        </li>
                    <?php
                     }
                     else
                     {
                         
                     }

                    ?>
                       
                </ul>
            </div>
        </div>
    </div>
    <!-- end sidebar menu -->