<?php
    include('header.php');
    include('side-bar.php');
?>
    <div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Stream Information</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Stream</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Stream Add</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Stream Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="grade-form" method="post" class="form-horizontal">
										<div class="form-body">
											
                    
										
											<div class="form-group row">
												<label class="control-label col-md-3" for="txtstream">Stream
													<span class="required"> * </span>
												</label>
                                                <div class="col-md-5">
													<select class="form-control input-height" name="txtstream" id="txtstream">
														<option value="">Select...</option>
														<option value="Bio">Bio</option>
														<option value="Maths">Maths</option>
														<option value="Commerce">Commerce</option>
														<option value="Arts">Arts</option>
														<option value="Eng-Technology">Eng-Technology</option>
														<option value="Bio-Technology">Bio-Technology</option>
														<option value="Other">Other</option>
													</select>
												</div>
											
                                            </div>
                                            
                                            <div class="form-group row" id="divother" hidden>
												<label class="control-label col-md-3" for="txtother">Other Stream Name
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtother" id="txtother" placeholder="Enter Sub Name" class="form-control input-height" />
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-3" for="txtsub1" id="txtsubname1" name="txtsubname1">Subject 1
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
                                                <select class="form-control input-height" name="txtsub1" id="txtsub1">
                                                <?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM subject ");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["name"].'">'. $row["name"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
													</select>												</div>
                                            </div>
                                        <div class="form-group row">
												<label class="control-label col-md-3" for="txtsub2" id="txtsubname2" name="txtsubname2">Subject 2
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
                                                <select class="form-control input-height" name="txtsub2" id="txtsub2">
                                                <?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM subject ");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["name"].'">'. $row["name"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
													</select>												</div>
                                            </div>
                                            <div class="form-group row">
												<label class="control-label col-md-3" for="txtsub3" id="txtsubname3" name="txtsubname3">Subject 3
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
                                                <select class="form-control input-height" name="txtsub3" id="txtsub3">
                                                <?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM subject ");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["name"].'">'. $row["name"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
                                                    </select>								
                                               </div>
                                            </div>
											
                        <div class="form-actions">
												<div class="row">
													<div class="offset-md-3 col-md-9">
														<button type="submit"  name="submit_grade" id="submit_grade" class="btn btn-info m-r-20">Submit</button>
														<button type="button" class="btn btn-default">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			 <div class="container">
  

 
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <div class="modal-body">
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
</div> 
<?php
include('footer.php');
//$receive = $_POST['data'];
?>

	<script>
	//for other stream sub menu
	function hiddenRemove(){
		if($('#txtstream').val() =="Other")
			{
				$('#divother').removeAttr('hidden');
			}
			else{
				$('#divother').attr("hidden","true");
			}
	}
// for save the data
	function save(){
		var result = "stream";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

		method:'POST',
		url:"load/save.php",
		data:data,
		dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			if(data=="Records inserted successfully.")
			{
				$.toast({
						heading: 'Stream Successfully Added',
						text: 'Data Added Successfully',
						position: 'top-right',
						loaderBg:'#ff6849',
						icon: 'success',
						hideAfter: 3500, 
						stack: 6
				});
			}
			else if(data=="Other Stream Name or Stream Already Added.")
			{
							$.toast({
								heading: 'Other Stream Name Already Added.',
								text: 'Record Already Added.',
								position: 'top-right',
								loaderBg:'#ff6849',
								icon: 'error',
								hideAfter: 3500
								
							});
			}
		console.log(data);
		$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
		$('select').val($('select option:first').val());

		hiddenRemove();



		})
		.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 

		});
	}
	//for required
	function required(){
					$.toast({
								heading: 'Please Fill The All Details.',
								text: 'All fileds are must.',
								position: 'top-right',
								loaderBg:'#ff6849',
								icon: 'error',
								hideAfter: 3500
								
					});
	}
	//same value for subject validation
	function samevalue(){
					$.toast({
								heading: 'Same Value for Subjects.',
								text: 'Please Select Different Subjects for Every Field.',
								position: 'top-right',
								loaderBg:'#ff6849',
								icon: 'error',
								hideAfter: 3500
								
					});
	}

	$(document).ready(function(){

	

		$('input[type=text]').css('text-transform','capitalize');
		$('#txtstream').change(function(){
			hiddenRemove();
		})
	

		$( "form" ).on( "submit", function( event ) {
				event.preventDefault();
				//required validation

				if($('#txtstream').val() =="Other")
				{
					if($('#txtstream').val() =="" || $('#txtsub1').val()=="" || $('#txtsub2').val()=="" || $('#txtsub3').val()==""|| $('#txtother').val()=="" )
					{
						required();
					}
					else
					{
						//same value for subject validation
						if( $('#txtsub1').val()==$('#txtsub2').val() || $('#txtsub1').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub1').val() ||$('#txtsub3').val()==$('#txtsub1').val() || $('#txtsub3').val()==$('#txtsub2').val())
						{
							samevalue();
						}
						else
						{
							save();
						}
						
					}
				}
				else{
					if($('#txtstream').val() =="" || $('#txtsub1').val()=="" || $('#txtsub2').val()=="" || $('#txtsub3').val()=="")
					{
						required();
					}
					else
					{
						//same value for subject validation
						if( $('#txtsub1').val()==$('#txtsub2').val() || $('#txtsub1').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub1').val() ||$('#txtsub3').val()==$('#txtsub1').val() || $('#txtsub3').val()==$('#txtsub2').val())
						{
							samevalue();
						}
						else
						{
							save();
						}
					}
				}

	})
			

	})
			
</script>  