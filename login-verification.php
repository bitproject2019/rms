<?php
include('header.php');
include('side-bar.php');

?>
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">User Account Details</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">User</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>User Account Details</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body " id="bar-parent">
									<table id="exportTable1" class="display nowrap" style="width:100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>Full Name</th>
												<th>Phone No</th>
                                                <th>User Name</th>
                                                <th>Status</th>
												<th>Action</th>
										
											</tr>
										</thead>
										<tbody id="tbody">

										</tbody>
										<tfoot>
											<tr>
                                                <th>ID</th>
												<th>Full Name</th>
												<th>Phone No</th>
                                                <th>User Name</th>
                                                <th>Status</th>
												<th>Action</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="container">	
	<div class="modal fade" id="myModal" role="dialog">
   		<div class="modal-dialog modal-lg">
      		<div class="modal-content">
     
       			 <div class="modal-body">
        			<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>User Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="grade-form" method="post" class="form-horizontal">
										<div class="form-body">
                                            <div class="form-group row" id="txtiddiv" >
                                                <label class="control-label col-md-4" for="txtid">ID
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtid" id="txtid" placeholder="Enter Sub Name" class="form-control input-height" readonly/>
												</div>
											</div>
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtfullname">Full Name
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="text" name="txtfullname" id="txtfullname" placeholder="Enter Full Name" class="form-control input-height" />
												</div>
                                            
                                        </div>
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtphone">Phone No
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="text" name="txtphone" id="txtphone" placeholder="Enter Phone No" class="form-control input-height num" />
												</div>
                                            
                                        </div>
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtnic">NIC
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="text" name="txtnic" id="txtnic" placeholder="Enter NIC No" class="form-control input-height" />
												</div>
                                            
                                        </div>
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtuser">Username
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="text" name="txtuser" id="txtuser" placeholder="Enter Username Name" class="form-control input-height" readonly />
												</div>
                                            
                                        </div>
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtverification">Verification
													<span class="required"> * </span>
												</label>
                                                <div class="col-md-5">
													<select class="form-control input-height" name="txtverification" id="txtverification">
														<option value="" >Select...</option>
														<option value="Inactive">Inactive</option>
														<option value="Active">Active</option>
														
													</select>
												</div>
											
                                            </div>
											<div class="form-group row">
												<label class="control-label col-md-4" for="txtusertype">User Type
													<span class="required"> * </span>
												</label>
                                                <div class="col-md-5">
													<select class="form-control input-height" name="txtusertype" id="txtusertype">
														<option value="" >Select...</option>
														<option value="User">User</option>
														<option value="Admin">Admin</option>
														
													</select>
												</div>
											
                                            </div>
										
                                            <div class="form-actions">
												<div class="row">
													<div class="offset-md-5 col-md-9">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<button type="submit"  name="submit_login" id="submit_login" class="btn btn-info m-r-20">Submit</button>
													</div>
												</div>
											</div>
										
										
										</div>
									</form>						
                                </div>
							</div>
						</div>
					</div>
				</div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
</div> 

<?php

    include('footer.php');
?>

<script>


// for save the data
	function save(){
        //alert('save');
		var result = "logupdate";

		var data = $('form').serialize()+'&result='+result;
		$.ajax({

			method:'POST',
			url:"load/update-login.php",
			data:data,
			dataType:"text",
			success:function(data){
				console.log(data);
				$.toast({
					heading: 'User Successfully Updated',
					text: 'Data Updated Successfully',
					position: 'top-right',
					loaderBg:'#ff6849',
					icon: 'success',
					hideAfter: 3500, 

					stack: 6
				});
				window.setTimeout(function(){location.reload()},1000);
				$('#myModal').modal('hide');
				hiddenRemove();
			}

		})
		
	}
	
function load(){
    var result = "viewlogin";
    $.ajax({

        method:'POST',
        url:"load/login-details.php",
        data:{result:result},
        dataType:"text",
        success:function(res){
            $('#tbody').html(res);
          

        }

    });
}
$(document).ready(function(){
	$(".num").forceNumeric();
    //load devisions details from view page
    dataTable();
    load();
//for update 
    $('#submit_login').click(function(e){
			
        e.preventDefault();	
        save();
        load();
    });

// open model
$(document).on('click', '.edit', function(){  

	var result = "fetchlogin";
    var id = $(this).attr("id");  
    $('#myModal').modal('show');
	//$(".modal-body").html($(".row").find('.card-box').html());
    $.ajax({  
        url:"load/login-details.php",  
        method:"POST",  
        data:{id:id,result:result},  
        dataType:"json",  
        success:function(data){  
            $('#txtid').val(data.id);  
            $('#txtfullname').val(data.fullname);  
            $('#txtphone').val(data.phone);  
            $('#txtuser').val(data.username);  
            $('#txtverification').val(data.status);  
			$('#txtnic').val(data.nic);        
			$('#txtusertype').val(data.user_type);      
        }  
    })
}); 
});
</script>

