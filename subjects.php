<?php
include('header.php');
include('side-bar.php');
$checkedtamil='';
$checkedenglish='';
?>

<!-- start page content -->
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Subjects Details</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Subject</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">All Subject</li>
							</ol>
						</div>
                    </div>
                    <div class="col-md-12 col-sm-12">
							<div class="card card-box">
							<div class="card-head">
									<header>All Subjects</header>
		
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body " id="bar-parent">
									<table id="exportTable1" class="display nowrap" style="width:100%">
										<thead>
											<tr>
												<th>Subject</th>
												<th>Medium</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="tbody">
											
										</tbody>
										<tfoot>
                                        	<tr>
                                       			<th>Subject</th>
												<th>Medium</th>
												<th>Action</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
					
<div class="container">	
	<div class="modal fade" id="myModal" role="dialog">
   		<div class="modal-dialog modal-lg">
      		<div class="modal-content">
     
       			 <div class="modal-body">
                    <div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Subject Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="submit-form" method="post" class="form-horizontal">
										<div class="form-body">
										<div class="form-group row" id="txtiddiv" hidden >
												
												<div class="col-md-5">
													<input type="text" name="txtid" id="txtid" placeholder="Enter Sub Name" class="form-control input-height" />
												</div>
											</div>
                                            <div class="form-group row">
												<label class="control-label col-md-3" for="txtsubject">Subject
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtsubject" id="txtsubject" placeholder="Enter Subject Name" class="form-control input-height" /> </div>
											</div>
                                            <div class="form-group row">
												<label class="control-label col-md-3" for="multiple">Medium
													<span class="required"> * </span>
												</label>
									
												<div class="col-md-5 ">
												<div class="checkbox checkbox-aqua">
													<input id="txtmedium" name="txtmedium" type="checkbox" <?php echo $checkedtamil; ?> value="Tamil">
													<label for="checkboxbg4">
															Tamil
													</label>
											
													<input id="txtmedium" type="checkbox" name="txtmedium" value="English" <?php echo $checkedenglish; ?>>
													<label for="checkboxbg4">
															English
													</label>
                         </div>
												</div>
																						</div>
																				
                                            <div class="form-actions">
												<div class="row">
													<div class="offset-md-3 col-md-9">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<button type="submit" name="submit-subject" id="submit-subject" class="btn btn-info m-r-20">Submit</button>
														
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
</div> 
			<!-- end page content -->
                
<?php
include('footer.php');
?>

<script>
 //for load
 function load(){
	
	var result = "viewsubject";
    $.ajax({

        method:'POST',
        url:"load/viewstream.php",      
        dataType:"text",
		data:{result:result},
        success:function(res){
			$('#tbody').html(res);
			//dataTable();
			
        }

    })
 }
// for save the data
	function save(){
        //alert('save');
		var result = "subjects";

		var medium =$('#multiple').serializeArray();
		var txtsubject =$('#txtsubject').val();
		var txtid =$('#txtid').val();
		var selectedLanguage = new Array();

		$('input[name="txtmedium"]:checked').each(function() {
			selectedLanguage.push(this.value);
		});
		var data ="&result="+result+"&selectedLanguage="+selectedLanguage+"&txtsubject="+txtsubject+"&txtid="+txtid;
		$.ajax({

			method:'POST',
			url:"load/update.php",
			data:data,
			dataType:"text",
			success:function(){
				console.log(data);
		
				$.toast({
					heading: 'Subject Successfully Updated',
					text: 'Data Updated Successfully',
					position: 'top-right',
					loaderBg:'#ff6849',
					icon: 'success',
					hideAfter: 3500, 

					stack: 6
				});
		
			
				$('#myModal').modal('hide');
				hiddenRemove();
				load();
			}

		});
		
	}

	//same value for subject validation
	function samevalue(){
        $.toast({
            heading: 'Same Value for Subjects.',
            text: 'Please Select Different Subjects for Every Field.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
                    
        });
	}
$(document).on('click', '.edit', function(){  
	var result = "fetchsubject";
    var id = $(this).attr("id");  
    $('#myModal').modal('show');
	//$(".modal-body").html($(".row").find('.card-box').html());
    $.ajax({  
        url:"load/viewstream.php",  
        method:"POST",  
        data:{id:id,result:result},  
        dataType:"json",  
        success:function(data){  
            $('#txtsubject').val(data.name);  
       
            if(data.tamil=="Tamil" && data.english=="English" ){
               // alert('okkkkkk');
                $(':checkbox[value=English]').prop('checked', true);
                $(':checkbox[value=Tamil]').prop('checked', true);
            }

            else if(data.english=="English")
            {
                
                $(':checkbox[value=English]').prop('checked', true);
                $(':checkbox[value=Tamil]').prop('checked', false);
           
            }
           else if(data.tamil=="Tamil")
            {
                $(':checkbox[value=Tamil]').prop('checked', true);
                $(':checkbox[value=English]').prop('checked', false);
          
            }
			$('#txtid').val(data.id);  
			
                
        }  
	});  
	
}); 

$(document).ready(function(){
	$('input[type=text]').css('text-transform','capitalize');
	load();
	var result = "viewsubject";
    $.ajax({

        method:'POST',
        url:"load/viewstream.php",      
        dataType:"text",
		data:{result:result},
        success:function(res){
			$('#tbody').html(res);
			dataTable();
			
        }

    })
	$('#submit-subject').click(function(e){
			
		e.preventDefault();
		
		if($('#txtsubject').val() =="" || $('#txtmedium').val()=="" )
		{
			required();
		}
		else
		{
			save();
			load();
		}
		
	})
});



</script>
