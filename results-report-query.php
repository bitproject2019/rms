<?php

include_once('load/connection.php');

//count all student
$output = '';
$date = date("Y");

$id = $_POST['id'];
$a =  substr($id, 0, 1);
$b =  substr($id, 1, 1);
$c =  substr($id, 2, 1);
$s =  substr($id, 3, 1);
$f =  substr($id, 4, 1);


if($_POST['stream']=='Bio-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Bio' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Maths-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Maths' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Commerce-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Commerce' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Arts-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Arts' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='etech-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Eng-Technology' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='btech-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Bio-Technology' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='other-first')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Other' and temp_results.year='$date' and temp_results.sitting='First' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}    
else if($_POST['stream']=='Bio-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Bio' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Maths-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Maths' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Commerce-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Commerce' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Arts-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Arts' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='etech-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Eng-Technology' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='btech-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Bio-Technology' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='other-second')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Other' and temp_results.year='$date' and temp_results.sitting='Second' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}    
else if($_POST['stream']=='Bio-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Bio' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Maths-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Maths' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Commerce-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Commerce' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='Arts-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Arts' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='etech-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Eng-Technology' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='btech-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Bio-Technology' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}

else if($_POST['stream']=='other-third')
{
    $sql = $mysqli->query("select temp_results.index_no,students.fullname, students.grade FROM temp_results INNER join students on students.index_no = temp_results.index_no where temp_results.a='$a' and temp_results.b='$b' and temp_results.c='$c' and temp_results.s='$s' and temp_results.f='$f' and temp_results.stream='Other' and temp_results.year='$date' and temp_results.sitting='Third' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
    
        $output .= ' 
        <tr class="odd gradeX">     
            <td class="center">' . $data['index_no'] . ' </td>
            <td class="center">' . $data['fullname'] . '</td>
            <td class="center">' . $data['grade'] . '</td>
         </tr>

        ';
      
    }
    echo $output;

}  
?>