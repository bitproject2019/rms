<?php
    include('header.php');
    include('side-bar.php');
?>
    <div class="page-content-wrapper">
				<div class="page-content" style="min-height: 558px;">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Grade Information</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Grade</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Grade Result</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Grade Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="grade-form" method="post" class="form-horizontal">
										<div class="form-body">
											
                      <div class="form-group row">
												<label class="control-label col-md-3" for="txtstream">Stream
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<select class="form-control input-height" name="txtstream" id="txtstream">
														<option value="">Select...</option>
														<option value="Bio">Bio</option>
														<option value="Maths">Maths</option>
														<option value="Commerce">Commerce</option>
														<option value="Arts">Arts</option>
														<option value="Eng-Technology">Eng-Technology</option>
														<option value="Bio-Technology">Bio-Technology</option>
														<option value="Other">Others</option>

                          </select>
												</div>
											</div>
											<div class="form-group row" id="divother" hidden>
												<label class="control-label col-md-3" for="txtother">Other Stream Name
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<select class="form-control input-height" name="txtother" id="txtother">
													<?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM stream where other_stream!=''");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["other_stream"].'">'. $row["other_stream"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-3" for="txtdivision">Division
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtdivision" id="txtdivision" placeholder="Enter Division" class="form-control input-height" maxlength="1"  />
												</div>
											</div>
											
											<div class="form-group row">
												<label class="control-label col-md-3" for="txtmedium">Medium
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<select class="form-control input-height" name="txtmedium" id="txtmedium" >
														<option value="">Select...</option>
														<option value="Tamil">Tamil</option>
														<option value="English">English</option>
													</select>
												</div>
                                            </div>
                        <div class="form-actions">
												<div class="row">
													<div class="offset-md-3 col-md-9">
														<button type="submit"  name="submit_grade" id="submit_grade" class="btn btn-info m-r-20">Submit</button>
														<button type="button" class="btn btn-default">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
<?php
include('footer.php');
?>

	<script>
	//for other stream sub menu
	function hiddenRemove(){
		if($('#txtstream').val() =="Other")
			{
				$('#divother').removeAttr('hidden');
			}
			else{
				$('#divother').attr("hidden","true");
			}
	}
// for save the data
	function save(){
		var result = "grade";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

		method:'POST',
		url:"load/save.php",
		data:data,
		dataType:"text",

		})
		.done(function (data) { 
			$.toast({
			heading: 'Grade SuccessfullyAdded',
			text: 'Data Added Successfully',
			position: 'top-right',
			loaderBg:'#ff6849',
			icon: 'success',
			hideAfter: 3500, 
			stack: 6
		});
		console.log(data);
		$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
		$('select').val($('select option:first').val());

		// $("#txtmedium").val($("#txtmedium option:first").val());
		// $("#txtstream").val($("#txtstream option:first").val());
		// $("#txtother").val($("#txtother option:first").val());
		hiddenRemove();



		})
		.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 

		});
	}
	//for required
	function required(){
					$.toast({
								heading: 'Please Fill The All Details.',
								text: 'All fileds are must.',
								position: 'top-right',
								loaderBg:'#ff6849',
								icon: 'error',
								hideAfter: 3500
								
					});
	}


	$(document).ready(function(){
		$('input[type=text]').css('text-transform','capitalize');
		$('#txtstream').change(function(){
			hiddenRemove();
		})
	

		$( "form" ).on( "submit", function( event ) {
				event.preventDefault();
				//required validation

				if($('#txtstream').val() =="Other")
				{
					if($('#txtstream').val() =="" || $('#txtmedium').val()=="" || $('#txtdivision').val()=="" || $('#txtother').val()=="" )
					{
						required();
					}
					else
					{
						save();
					}
				}
				else{
					if($('#txtstream').val() =="" || $('#txtmedium').val()=="" || $('#txtdivision').val()=="" )
					{
						required();
					}
					else
					{
						save();
					}
				}

	})
			

	})
			
</script>  