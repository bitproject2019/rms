<?php
include('header.php');
include('side-bar.php');

?>

<!-- start page content -->
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">All Streams</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Stream</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">All Stream</li>
							</ol>
						</div>
                    </div>
                    <div class="col-md-12 col-sm-12">

							<div class="card card-box">
								<div class="card-head">
									<header>All Stream</header>
		
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body " id="bar-parent">
									<table id="exportTable1" class="display nowrap" style="width:100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>Name</th>
												<th>Subject 1</th>
												<th>Subject 2</th>
												<th>Subject 3</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="tbody">
											
										</tbody>
										<tfoot>
                                        <tr>
												<th>ID</th>
												<th>Name</th>
												<th>Subject 1</th>
												<th>Subject 2</th>
												<th>Subject 3</th>
												<th>Action</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
					
<div class="container">	
	<div class="modal fade" id="myModal" role="dialog">
   		<div class="modal-dialog modal-lg">
      		<div class="modal-content">
     
       			 <div class="modal-body">
        			<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Stream Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton" data-dismiss="modal">
                                    <i class="material-icons f-left">clear_all</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="grade-form" method="post" class="form-horizontal">
										<div class="form-body">
										<div class="form-group row" id="txtiddiv" hidden >
												
												<div class="col-md-5">
													<input type="text" name="txtid" id="txtid" placeholder="Enter Sub Name" class="form-control input-height" />
												</div>
											</div>
                    
										
											<div class="form-group row">
												<label class="control-label col-md-4" for="txtstream">Stream
													<span class="required"> * </span>
												</label>
                                                <div class="col-md-5">
													<select class="form-control input-height" name="txtstream" id="txtstream" readonly>
														<option value="">Select...</option>
														<option value="Bio">Bio</option>
														<option value="Maths">Maths</option>
														<option value="Arts">Arts</option>
														<option value="Commerce">Commerce</option>
														<option value="Eng-Technology">Eng-Technology</option>
														<option value="Bio-Technology">Bio-Technology</option>
														<option value="Other">Other</option>
													</select>
												</div>
											
                                            </div>
                                            
                                            <div class="form-group row" id="divother" hidden>
												<label class="control-label col-md-4" for="txtother">Other Stream Name
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtother" id="txtother" placeholder="Enter Sub Name" class="form-control input-height"  readonly/>
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-4" for="txtsub1" id="txtsubname1" name="txtsubname1">Subject 1
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
                                                <select class="form-control input-height" name="txtsub1" id="txtsub1">
                                                <?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM subject ");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["name"].'">'. $row["name"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
													</select>												</div>
                                            </div>
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtsub2" id="txtsubname2" name="txtsubname2">Subject 2
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
                                                <select class="form-control input-height" name="txtsub2" id="txtsub2">
                                                <?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM subject ");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["name"].'">'. $row["name"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
													</select>												</div>
                                            </div>
                                            <div class="form-group row">
												<label class="control-label col-md-4" for="txtsub3" id="txtsubname3" name="txtsubname3">Subject 3
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
                                                <select class="form-control input-height" name="txtsub3" id="txtsub3">
                                                <?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM subject ");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["name"].'">'. $row["name"] .'</option>';
												}
												echo $output;
											?>
                                                    </select>								
                                               </div>
                                            </div>
											
                       						 <div class="form-actions">
												<div class="row">
													<div class="offset-md-4 col-md-9">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<button type="button"  name="submit_grade" id="submit_grade" class="btn btn-info m-r-20">Submit</button>
														
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
</div> 
			<!-- end page content -->
                
<?php
include('footer.php');
?>

<script>
// for save the data
	function save(){
        //alert('save');
		var result = "stream";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

			method:'POST',
			url:"load/update.php",
			data:data,
			dataType:"text",
			
		})
		.done(function (data) { 
			console.log(data);
		
			$.toast({
				heading: 'Stream Successfully Updated',
				text: 'Data Updated Successfully',
				position: 'top-right',
				loaderBg:'#ff6849',
				icon: 'success',
				hideAfter: 3500, 

				stack: 6
			});
			//window.setTimeout(function(){location.reload()},1000);
			load();
			$('#myModal').modal('hide');
			hiddenRemove();
		})
		.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 

		});
	}

	//same value for subject validation
	function samevalue(){
        $.toast({
            heading: 'Same Value for Subjects.',
            text: 'Please Select Different Subjects for Every Field.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
                    
        });
	}
$(document).on('click', '.edit', function(){  
	var result = "fetch";
    var id = $(this).attr("id");  
    $('#myModal').modal('show');
	//$(".modal-body").html($(".row").find('.card-box').html());
    $.ajax({  
        url:"load/viewstream.php",  
        method:"POST",  
        data:{id:id,result:result},  
        dataType:"json",  
        success:function(data){  
			$('#txtstream').val(data.name);  
			$('#txtsub1').val(data.subject1);  
			$('#txtsub2').val(data.subject2);  
			$('#txtsub3').val(data.subject3);  
			$('#txtother').val(data.other_stream);  

			$('#txtid').val(data.id);  
			if(data.name=="Other")
			{
				hiddenRemove();
			}
			else{
				$('#divother').attr("hidden","true");

			}
		
                
        }  
	});  
	
}); 
function load(){
	var result = "view";
    $.ajax({

        method:'POST',
        url:"load/viewstream.php",      
        dataType:"text",
		data:{result:result},
        success:function(res){
			$('#tbody').html(res);
			
        }

    })
}
$(document).ready(function(){
	$('input[type=text]').css('text-transform','capitalize');
//for load data
	//dataTable();
	load();
	var result = "view";
    $.ajax({

        method:'POST',
        url:"load/viewstream.php",      
        dataType:"text",
		data:{result:result},
        success:function(res){
			$('#tbody').html(res);
			dataTable();
        }

    });
	//========================================
	//for update data

	$('#submit_grade').click(function(){
			
		if($('#txtstream').val() =="Other")
		{
			if($('#txtstream').val() =="" || $('#txtsub1').val()=="" || $('#txtsub2').val()=="" || $('#txtsub3').val()==""|| $('#txtother').val()=="" )
			{
				required();
			}
			else
			{
				//same value for subject validation
				if( $('#txtsub1').val()==$('#txtsub2').val() || $('#txtsub1').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub1').val() ||$('#txtsub3').val()==$('#txtsub1').val() || $('#txtsub3').val()==$('#txtsub2').val())
				{
					//console.log("same");
					samevalue();
				}
				else
				{
					save();
					load();
				
				}
				
			}
		}
		else{
			if($('#txtstream').val() =="" || $('#txtsub1').val()=="" || $('#txtsub2').val()=="" || $('#txtsub3').val()=="")
			{
				required();
			}
			else
			{
				//same value for subject validation
				if( $('#txtsub1').val()==$('#txtsub2').val() || $('#txtsub1').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub3').val() || $('#txtsub2').val()==$('#txtsub1').val() ||$('#txtsub3').val()==$('#txtsub1').val() || $('#txtsub3').val()==$('#txtsub2').val())
				{
					//console.log("same");
					samevalue();
				}
				else
				{
					save();
					load();

				}
			}
		}
	})
});



</script>
