<?php
include('header.php');
include('side-bar.php');
?>

<?php


    
?>



<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Pass Percentage Report </div>
					
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Result</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					
					
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Pass Percentage</header>
						<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
							<i class="material-icons">more_vert</i>
						</button>
						<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
							<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
							<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
							<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
						</ul>
					</div>
					<div class="card-body" id="bar-parent">
                        <div class="row">
                    <div class="col-sm-12">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> 
                    
                        <thead>
                        
                            <tr>
                                <th>Stream</th>
                                <th>3F</th>
                                <th>2F</th>
                                <th>F</th>
                                <th>Above 3S</th>
                                <th>Pass %</th>
                            </tr>
                        </thead>
                        <tbody>';
                    $sql = $mysqli->query("select distinct(name) FROM stream");

                    while ($data = $sql->fetch_array() ) {
                    

                        $sql1 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date' and temp_results.stream='$data[name]' and temp_results.f='3' ");

                        while ($data1 = $sql1->fetch_array() ) {

                            $sql2 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date' and temp_results.stream='$data[name]' and temp_results.f='2' ");

                            while ($data2 = $sql2->fetch_array() ) {

                                $sql3 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date' and temp_results.stream='$data[name]' and temp_results.f='1' ");

                                while ($data3 = $sql3->fetch_array() ) {
    
                                    $sql4 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date' and temp_results.stream='$data[name]' and temp_results.f='0' ");

                                    while ($data4 = $sql4->fetch_array() ) {
        
                                        $sql5 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results where temp_results.year='$date' and temp_results.stream='$data[name]' ");

                                        while ($data5 = $sql5->fetch_array() ) {

                                            if($data5['count']!=0)
                                            {
                                                $percentage = round(($data4['count']/$data5['count'] * 100),2);
            
                                                echo '<tr><td>'.$data['name'].'</td><td>'.$data1['count'].'</td><td>'.$data2['count'].'</td><td>'.$data3['count'].'</td><td>'.$data4['count'].'</td><td>'.$percentage.'%</td></tr>';

                                                
                                                

                                            }
                                            else{
                                                //$percentage = ($data4['count']/$data5['count'] * 100);
            
                                                echo '<tr><td>'.$data['name'].'</td><td>'.$data1['count'].'</td><td>'.$data2['count'].'</td><td>'.$data3['count'].'</td><td>'.$data4['count'].'</td><td>0%</td></tr>';
                                            }
                                           

                                           
    
                                    
                                        }
                                       
                                                                      
                                    }
                            
                                }
                        
                            }

                                                   
                       
                        }
                     }

                     
                     $sql1 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date' and temp_results.f='3' ");

                     while ($data1 = $sql1->fetch_array() ) {

                         $sql2 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date' and temp_results.f='2' ");

                         while ($data2 = $sql2->fetch_array() ) {

                             $sql3 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date'  and temp_results.f='1' ");

                             while ($data3 = $sql3->fetch_array() ) {
 
                                 $sql4 = $mysqli->query("select count(temp_results.f) as count FROM temp_results where temp_results.year='$date'  and temp_results.f='0' ");

                                 while ($data4 = $sql4->fetch_array() ) {
     
                                     $sql5 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results where temp_results.year='$date'");

                                     while ($data5 = $sql5->fetch_array() ) {

                                         if($data5['count']!=0)
                                         {
                                             $percentage = round(($data4['count']/$data5['count'] * 100),2);
         
                                             echo '<tr><td>Total</td><td>'.$data1['count'].'</td><td>'.$data2['count'].'</td><td>'.$data3['count'].'</td><td>'.$data4['count'].'</td><td>'.$percentage.'%</td></tr>';

                                             
                                             

                                         }
                                         else{
                                             //$percentage = ($data4['count']/$data5['count'] * 100);
         
                                             echo '<tr><td>Total</td><td>'.$data1['count'].'</td><td>'.$data2['count'].'</td><td>'.$data3['count'].'</td><td>'.$data4['count'].'</td><td>0%</td></tr>';
                                         }
                                        

                                        
 
                                 
                                     }
                   
                             
                                 }
                         
                             }
                     
                         }

                                                
                    
                     }
          
                    
                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                  
				</div>
			</div>
		</div>
	</div>
</div>
</div>

</div>

<?php

    include('footer.php');

?>
