<?php
include('header.php');
include('side-bar.php');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Result Subject Details Third Attempt </div>
					
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Result</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					
					
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Result Subject Details</header>
						<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
							<i class="material-icons">more_vert</i>
						</button>
						<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
							<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
							<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
							<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
						</ul>
					</div>
					<div class="card-body" id="bar-parent">
                        <div class="row">
                    <div class="col-sm-12">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> 
                    
                        <thead>
                        
                            <tr>
                                <th>Subjects</th>
                                <th>A</th>
                                <th>B</th>
                                <th>C</th>
                                <th>S</th>
                                <th>F</th>
                            </tr>
                        </thead>
                        <tbody>';
                    $sql = $mysqli->query("select * FROM subject");

                    while ($data = $sql->fetch_array() ) {
                    
                       //echo '<tr>  <td>'.$data['name'].'</td></tr>';

                        $sql1 = $mysqli->query("select count(results.result) as count, results.result FROM results inner join students on students.index_no = results.index_no where results.year='$date' and results.subject_id='$data[name]' and results.result='A' and students.sitting='Third'  ");

                                       //         echo '<tr><td>'.$data1['count'].'-'.$data1['result'].'-'.$data['name'].'</td></tr>';


                        while ($data1 = $sql1->fetch_array() ) {


                            $sql2 = $mysqli->query("select count(results.result) as count, results.result FROM results inner join students on students.index_no = results.index_no where results.year='$date' and results.subject_id='$data[name]' and results.result='B' and students.sitting='Third'  ");

                            //         echo '<tr><td>'.$data1['count'].'-'.$data1['result'].'-'.$data['name'].'</td></tr>';


                            while ($data2 = $sql2->fetch_array() ) {

                                $sql3 = $mysqli->query("select count(results.result) as count, results.result FROM results inner join students on students.index_no = results.index_no where results.year='$date' and results.subject_id='$data[name]' and results.result='C' and students.sitting='Third'  ");

                                //         echo '<tr><td>'.$data1['count'].'-'.$data1['result'].'-'.$data['name'].'</td></tr>';
    
    
                                while ($data3 = $sql3->fetch_array() ) {

                                    $sql4 = $mysqli->query("select count(results.result) as count, results.result FROM results inner join students on students.index_no = results.index_no where results.year='$date' and results.subject_id='$data[name]' and results.result='S' and students.sitting='Third'  ");

                                    //         echo '<tr><td>'.$data1['count'].'-'.$data1['result'].'-'.$data['name'].'</td></tr>';
        
        
                                    while ($data4 = $sql4->fetch_array() ) {


                                        $sql5 = $mysqli->query("select count(results.result) as count, results.result FROM results inner join students on students.index_no = results.index_no where results.year='$date' and results.subject_id='$data[name]' and results.result='F' and students.sitting='Third'  ");

                                        //         echo '<tr><td>'.$data1['count'].'-'.$data1['result'].'-'.$data['name'].'</td></tr>';
            
            
                                        while ($data5 = $sql5->fetch_array() ) {
                                            echo '<tr><td>'.$data['name'].'</td><td>'.$data1['count'].'</td><td>'.$data2['count'].'</td><td>'.$data3['count'].'</td><td>'.$data4['count'].'</td><td>'.$data5['count'].'</td></tr>';
                                            
                                            //echo ($data['a']!=0?($data['a']!=1?$data['a'].'A ':'A'):'');
                                            
                                        } 
                                      
                                        
                                    } 
                                
                                    
                                } 

                                
                            } 

                           
                        } 
                     
                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                  
				</div>
			</div>
		</div>
	</div>
			</div>
            </div>

            </div>

<?php

include('footer.php');

?>
