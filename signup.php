<!DOCTYPE html>
<html>


<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="RedstarHospital" />
    <title>Vembadi Girls' High School</title>
    <!-- Jquery Toast css -->
	<link rel="stylesheet" href="assets/plugins/jquery-toast/dist/jquery.toast.min.css">
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- google font -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../assets/plugins/iconic/css/material-design-iconic-font.min.css">
	<!-- bootstrap -->
	<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- style -->
	<link rel="stylesheet" href="../assets/css/pages/extra_pages.css">
	<!-- favicon -->
	<link rel="shortcut icon" href="../assets/img/favicon.ico" />
</head>

<body>
	<div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" id="signup-form">
					<span class="login100-form-logo">
						<img alt="" src="assets/img/Vembadi.png">
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Registration
					</span>
					<div class="row">
						<div class="col-lg-6 p-t-20">
							<div class="wrap-input100 validate-input" data-validate="Enter Full Name">
								<input class="input100" type="text" name="txtfullname" id="txtfullname" placeholder="Full Name">
								<span class="focus-input100" data-placeholder="&#xf207;"></span>
							</div>
                        </div>
                        <div class="col-lg-6 p-t-20">
							<div class="wrap-input100 validate-input" data-validate="Enter Mobile Number">
								<input class="input100 num" type="text" name="txtmobile" id="txtmobile" placeholder="Mobile">
								<span class="focus-input100" data-placeholder="&#xf129;"></span>
							</div>
                        </div>
                        <div class="col-lg-6 p-t-20">
							<div class="wrap-input100 validate-input" data-validate="Enter NIC No">
								<input class="input100" type="text" name="txtnic" id="txtnic" placeholder="NIC No">
								<span class="focus-input100" data-placeholder="&#xf129;"></span>
							</div>
                        </div>
                        <div class="col-lg-6 p-t-20">
							<div class="wrap-input100 validate-input" data-validate="Enter Username">
								<input class="input100" type="text" name="txtusername"  id="txtusername" placeholder="Username" readonly>
								<span class="focus-input100" data-placeholder="&#xf207;"></span>
							</div>
						</div>
					
						<div class="col-lg-6 p-t-20">
							<div class="wrap-input100 validate-input" data-validate="Enter Password">
								<input class="input100" type="password" name="txtpass" id="txtpass" placeholder="Password" minlength="6">
								<span class="focus-input100" data-placeholder="&#xf191;"></span>
							</div>
						</div>
						<div class="col-lg-6 p-t-20">
							<div class="wrap-input100 validate-input" data-validate="Enter Password again">
								<input class="input100" type="password" name="txtcpass" id="txtcpass" placeholder="Confirm password" >
								<span class="focus-input100" data-placeholder="&#xf191;"></span>
							</div>
						</div>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" name="signup-button" id="signup-button">
							Register
						</button>
					</div>
					<div class="text-center p-t-30">
						<a class="txt1" href="login">
							You already have a membership?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- start js include path -->
	<script src="../assets/plugins/jquery/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/pages/extra-pages/pages.js"></script>
    <script src="assets/plugins/material/material.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/jquery.toast.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/toast.js"></script>
	<!-- end js include path -->
</body>


<script>

// for save the data
	function save(){
		var result = "signup";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

            method:'POST',
            url:"load/save.php",
            data:data,
            dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			if(data=="Records inserted successfully.")
			{
				$.toast({
                    heading: 'User Added Successfully',
                    text: 'Wait for getting activated',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
				});
			}
			else if(data=="User already exist.")
			{
                $.toast({
                    heading: 'User Already Exist.',
                    text: 'Record Already Added.',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                    
                });
			}
            console.log(data);
            $('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
     

		});
		
	}
	//for required
	function required(){
		$.toast({
			heading: 'Please Fill The All Details.',
			text: 'All fileds are must.',
			position: 'top-right',
			loaderBg:'#ff6849',
			icon: 'error',
			hideAfter: 3500
					
		});
	}
	//same value for subject validation
	function samevalue(){
		$.toast({
			heading: 'Same User ID.',
			text: 'Username Already Exits.',
			position: 'top-right',
			loaderBg:'#ff6849',
			icon: 'error',
			hideAfter: 3500
					
		});
	}
	function passwordmiss(){
		$.toast({
			heading: 'Password Error.',
			text: 'Please Check Your Passwords.',
			position: 'top-right',
			loaderBg:'#ff6849',
			icon: 'error',
			hideAfter: 3500
					
		});
	}
	jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }

$(document).ready(function(){

	$(".num").forceNumeric();
    $('input[type=text]').css('text-transform','capitalize');

    $('#txtnic').keyup(function(){
       
        $('#txtusername').val($('#txtnic').val());
    })
    $( "form" ).on( "submit", function( event ) {
            event.preventDefault();
            //required validation
            if($('#txtpass').val() == $('#txtcpass').val()!='')
            {
                if($('#txtfullname').val()!='' && $('#txtmobile').val()!='' && $('#txtnic').val()!=''&& $('#txtusername').val()!=''&& $('#txtpass').val()!=''&& $('#txtcpass').val()!='' )
                {
                    save();
                }
                else
                {
                    required();
                }
            }
            else
            {
                passwordmiss();
            }
 

    })
			

})
			
</script>  
</html>