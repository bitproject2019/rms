<!DOCTYPE html>
<html>


<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="RedstarHospital" />
    <title>Vembadi Girls' High School</title>
        <!-- Jquery Toast css -->
	<link rel="stylesheet" href="assets/plugins/jquery-toast/dist/jquery.toast.min.css">
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- google font -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../assets/plugins/iconic/css/material-design-iconic-font.min.css">
	<!-- bootstrap -->
	<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- style -->
	<link rel="stylesheet" href="../assets/css/pages/extra_pages.css">
	<!-- favicon -->
	<link rel="shortcut icon" href="../assets/img/favicon.ico" />
</head>

<body>
	<div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" id="form-login">
					<span class="login100-form-logo">
                    <img alt="" src="assets/img/Vembadi.png">
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>
					<div class="wrap-input100 validate-input" data-validate="Enter username">
                        <input class="input100" type="text" name="txtusername" id="txtusername" placeholder="Username" >
								
						<span class="focus-input100" data-placeholder="&#xf207;"> </span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="txtpass" id="txtpass" placeholder="Password" >
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
				
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id="submit-login" name="submit-login">
							Login
						</button>
					</div>
					<div class="text-center p-t-30">
						<a class="txt1" href="forgot-password.php">
							Forgot Password?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- start js include path -->
	<script src="../assets/plugins/jquery/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/pages/extra-pages/pages.js"></script>
    <script src="assets/plugins/material/material.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/jquery.toast.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/toast.js"></script>
	<!-- end js include path -->
</body>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script>

// for save the data
	function save(){
		var result = "login";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

            method:'POST',
            url:"load/login.php",
            data:data,
            dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			if(data=="Login Success")
			{
                
				$.toast({
                    heading: 'User Added Successfully',
                    text: 'Wait for getting activated',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
				});

				 
                window.location.assign('home');

			}
			else if(data=="please fill proper details")
			{
                $.toast({
                    heading: 'User Account Error',
                    text: 'Check the Account Details',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                    
                });
			}
			else if(data=="Wait until get activated")
			{
                $.toast({
                    heading: 'User Account Not Activated',
                    text: 'Wait until get activated',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                    
                });
			}
            console.log(data);
            $('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
     

		});
		
	}
	//for required
	function required(){
        $.toast({
            heading: 'Please Fill The All Details.',
            text: 'All fileds are must.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
                    
        });
	}
	//same value for subject validation

	

$(document).ready(function(){
   // $('input[type=text]').css('text-transform','capitalize');

   
    $( "form" ).on( "submit", function( event ) {
            event.preventDefault();
            //required validation
             if($('#txtusername').val()!=''&& $('#txtpass').val()!='' )
                {
                    save();
                }
                else
                {
                    required();
                }
        

    })
			

})
			
</script>  
</html>