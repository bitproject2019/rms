<?php
include('header.php');
include('side-bar.php');
?>


<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Result Details </div>
					
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Result</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Add Result</li>
					
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Basic Information</header>
						<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
							<i class="material-icons">more_vert</i>
						</button>
						<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
							<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
							<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
							<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
						</ul>
					</div>
					<div class="card-body" id="bar-parent">
						<form  id="results-form" class="form-horizontal" method="post" >
							<div class="form-body">
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtregno">Reg No
										<span class="required">  </span>
									</label>
									<div class="col-md-5">
										<input type="text" name="txtregno" id="txtregno" placeholder="Enter Reg No" class="form-control input-height" />
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtfullname">Full Name
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input type="text" name="txtfullname" id="txtfullname" placeholder="Enter Fullname" class="form-control input-height textonly" onblur="validate()" />
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtindex">Index No
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input type="text" name="txtindex" id="txtindex" placeholder="Enter Index" class="form-control input-height" /> </div>
								</div>
								
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtyear">Year
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input type="text" id="txtyear" name="txtyear" placeholder="Select Year" class="form-control yearpicker input-height" /> 
									</div>
									
								</div>
								
							
																			
									<div class="form-group row">
									<label class="control-label col-md-3" for="txtsitting">Sitting
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<select class="form-control input-height"  id="txtsitting" name="txtsitting">
											<option value="First">First </option>
											<option value="Second">Second</option>
											<option value="Third">Third</option>
								
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtstream">Stream
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<select class="form-control input-height" name="txtstream" id="txtstream">
											<option value="">Select...</option>
											<option value="Bio">Bio</option>
											<option value="Maths">Maths</option>
											<option value="Commerce">Commerce</option>
											<option value="Arts">Arts</option>
											<option value="Eng-Technology">Eng-Technology</option>
											<option value="Bio-Technology">Bio-Technology</option>
											<option value="Other">Other</option>
										</select>
									</div>
								</div>
								<div class="form-group row" id="divother" hidden>
									<label class="control-label col-md-3" for="txtother">Other Stream
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<select class="form-control input-height" name="txtother" id="txtother">
										<?php
									
									include_once('load/connection.php');
									//$output = '';
									$sql = $mysqli->query("SELECT * FROM stream where other_stream!=''");
									$output = '<option value="">Select...</option>';
									while ($row = $sql->fetch_array()) {
										$output .= '<option value="'.$row["other_stream"].'">'. $row["other_stream"] .'</option>';
									}
									//AND travelers_room_status.room_type='" . $_POST['room'] . "'
									echo $output;
								?>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtmedium">Medium
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<select class="form-control input-height" name="txtmedium" id="txtmedium">
											<option value="">Select...</option>
											<option value="Tamil">Tamil</option>
											<option value="English">English</option>
										</select>
									</div>
									</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtgrade">Division
										<span class="required">  </span>
									</label>
									<div class="col-md-5">
										<select class="form-control input-height"  id="txtgrade" name="txtgrade">
										<option value="">Select...</option>

										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtsub1" id="txtsubname1" name="txtsubname1">			
									Subject 1
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtsub1" id="txtsub1" type="text" placeholder="Enter Result" class="form-control input-height text"  maxlength="1" onblur="validate()" />
									</div>
																			</div>
																	<div class="form-group row">
									<label class="control-label col-md-3" for="txtsub2" id="txtsubname2" name="txtsubname2">Subject 2
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtsub2" id="txtsub2" type="text" placeholder="Enter Result" class="form-control input-height text" maxlength="1" onblur="validate()" />
									</div>
																			</div>
																			<div class="form-group row">
									<label class="control-label col-md-3" for="txtsub3" id="txtsubname3" name="txtsubname3">Subject 3
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtsub3" id="txtsub3" type="text" placeholder="Enter Result" class="form-control input-height text" maxlength="1" onblur="validate()"/>
									</div>
																			</div>
																			<div class="form-group row">
									<label class="control-label col-md-3" for="txtgk" id="txtsubname4" name="txtsubname4">GK
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtgk" id="txtgk" type="text" placeholder="Enter Result" class="form-control input-height num" maxlength="3" />
									</div>
																			</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtenglish" id="txtsubname5" name="txtsubname5" >General English
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtenglish" id="txtenglish" type="text" placeholder="Enter Result" class="form-control input-height text" maxlength="1" onblur="validate()" />
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtrank" >District Rank
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtrank" id="txtrank" type="text" placeholder="Enter District Rank" class="form-control input-height num"  />
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3" for="txtrank" >Island Rank
										<span class="required"> * </span>
									</label>
									<div class="col-md-5">
										<input name="txtirank" id="txtirank" type="text" placeholder="Enter Island Rank" class="form-control input-height num"  />
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="offset-md-3 col-md-9">
											<button type="submit" name="results_submit" id="results_submit" class="btn btn-info m-r-20">Submit</button>
											<button type="button" class="btn btn-default">Cancel</button>
	
											
										</div>
									</div>
								</div>
											
										</div>
									</div>
								</div>
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
			</div>


           
<?php
include('footer.php');
?>


<script>
//for chnage the visibility of the other stream sub category name
	function hiddenRemove(){
		if($('#txtstream').val() =="Other")
			{
				$('#divother').removeAttr('hidden');
			}
			else{
				$('#divother').attr("hidden","true");
			}
	}
	//for passing the form value to save
	function save(){
				var subject1 = $('#txtsubname1').text();
				var subject2 = $('#txtsubname2').text();
				var subject3 = $('#txtsubname3').text();
				var subject4 = $('#txtsubname4').text();
				var subject5 = $('#txtsubname5').text();
				var result = "result";
				var data = $('form').serialize()+"&subject1="+subject1+"&subject2="+subject2+"&subject3="+subject3+"&subject4="+subject4+"&subject5="+subject5+"&result="+result;
				//console.log(data);
		$.ajax({

			method:'POST',
			url:"load/save.php",
			data:data,
			dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			$.toast({
				heading: 'Result Successfully Added',
				text: 'Data Added Successfully.',
				position: 'top-right',
				loaderBg:'#ff6849',
				icon: 'success',
				hideAfter: 2000, 
				stack: 6
			});
			window.setTimeout(function(){location.reload()},1000);
			//clear all field
			$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
			//$('select').val($('select option:first').val());
			//$('form').val();
			$("#txtgrade").val($("#txtgrade option:first").val());
			$("#txtmedium").val($("#txtmedium option:first").val());
			$("#txtstream").val($("#txtstream option:first").val());
			$("#txtother").val($("#txtother option:first").val());
			$("#txtsitting").val($("#txtsitting option:first").val());
			hiddenRemove();
		})
		.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 
			})
			.done(function (data) { 
								console.log(data);
								if(data=="Student Already Added.")
								{
										$.toast({
											heading: 'Student Already Added.',
											text: 'Record Already Added.',
											position: 'top-right',
											loaderBg:'#ff6849',
											icon: 'error',
											hideAfter: 3500
									
										});

								}
								else if(data=="Records inserted successfully.")
								{
									// $.toast({
									// 	heading: 'Result Successfully Added',
									// 	text: 'Data Added Successfully.',
									// 	position: 'top-right',
									// 	loaderBg:'#ff6849',
									// 	icon: 'success',
									// 	hideAfter: 3500, 
									// 	stack: 6
									// });

								}
								
							//clear all field

								$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
								//$('select').val($('select option:first').val());
								//$('form').val();
								$("#txtgrade").val($("#txtgrade option:first").val());
								$("#txtmedium").val($("#txtmedium option:first").val());
								$("#txtstream").val($("#txtstream option:first").val());
								$("#txtother").val($("#txtother option:first").val());
								$("#txtsitting").val($("#txtsitting option:first").val());
								hiddenRemove();



			})
			.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 

		})

		

	}
	//for required validation
	function required(){
						// var textBox =  $.trim( $('input[type=text]').val() )
						// if (textBox == "") {
							$.toast({
								heading: 'Please Fill The All Details.',
								text: 'All fileds are must.',
								position: 'top-right',
								loaderBg:'#ff6849',
								icon: 'error',
								hideAfter: 3500
								
							});
						// }
	}


$(document).ready(function(){
		$('input[type=text]').css('text-transform','capitalize'); // capitalize case

		$(".num").forceNumeric();
		

		$('.text').on('keypress', function(key) {
			if((key.charCode < 97 || key.charCode > 99 )&& (key.charCode < 65 || key.charCode > 67) && (key.charCode != 45)) {
				if((key.charCode == 102)|| (key.charCode == 70) || (key.charCode == 83) ||(key.charCode == 115))
				{
					return true;
				}
				
				return false;	
			}
		});
		$('.textonly').on('keypress', function(key) {
			if((key.charCode < 97 || key.charCode > 122 )&& (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45)) {
				return false;	
			}
		});



//for grade
$('#txtmedium').change(function(){

var medium = $(this).val();
var stream = $('#txtstream').val();

if(stream == "Other")
{
	
					var other = $('#txtother').val();
					$.ajax({ 
						type: "POST", 
						url: "load/grade.php", 
						data: {stream:stream,medium:medium,other:other}, 
						success: function(data){ 
						
							$("#txtgrade").html(data); 
					//	console.log(data);
						}
					});

}
else
{
	//var other = $('#txtother').val();
	var other = 'not';
					$.ajax({ 
						type: "POST", 
						url: "load/grade.php", 
						data: {stream:stream,medium:medium,other:other}, 
						success: function(data){ 
						
							$("#txtgrade").html(data); 
					//	console.log(data);
						}
					});


}

})
		$('#txtstream').change(function(){
			hiddenRemove();
		})
	

		$( "form" ).on( "submit", function( event ) {
				event.preventDefault();
				//text box validation 
				
				if($('#txtstream').val() =="Other"){
						if($('#txtgrade').val()=="" ||$('#txtmedium').val()==""||$('#txtstream').val()==""||$('#txtother').val()==""||$('#txtfullname').val()==""||$('#txtindex').val()==""||$('#txtyear').val()==""||$('#txtsub1').val()==""||$('#txtsub2').val()==""||$('#txtsub3').val()==""||$('#txtenglish').val()==""||$('#txtgk').val()=="")
						{
							required();
							//console.log("Select the value for all");
						}
						else
						{
							save();
							
					
					 	}
				}
				else
				{
					if($('#txtgrade').val()=="" ||$('#txtmedium').val()==""||$('#txtstream').val()==""||$('#txtfullname').val()==""||$('#txtindex').val()==""||$('#txtyear').val()==""||$('#txtsub1').val()==""||$('#txtsub2').val()==""||$('#txtsub3').val()==""||$('#txtenglish').val()==""||$('#txtgk').val()=="")
					{
						
						//console.log("Select the value for all");
						required();
			
					}
					else
					{

						save();
						
				
					}
				}
			
		});

	});
</script> 

<script>
	//for change the text of the subject lable
$('#txtstream').change(function(){

	var stream = $(this).val();
	if(stream!="Other")
	{ /* GET THE VALUE OF THE SELECTED DATA */
			var sub1 = "subject1";
			var sub2 = "subject2";
			var sub3 = "subject3";
		// var dataString = "stream="+stream+"sub1="+sub1; 

					$.ajax({ 
						type: "POST", 
						url: "load/dropdown.php", 
						data: {stream:stream,sub1:sub1}, 
						success: function(data){ 
							//$("#txtsubname1").html(''); 
							$('label[id*="txtsubname1"]').text('');
							$("#txtsubname1").html(data+'<span class="required"> * </span>'); 
						console.log(data);
						}
					});
					
					$.ajax({ 
						type: "POST", 
						url: "load/dropdownsub2.php", 
						data: {stream:stream,sub2:sub2}, 
						success: function(data){ 
							//$("#txtsubname1").html(''); 
							$('label[id*="txtsubname2"]').text('');
							$("#txtsubname2").html(data+'<span class="required"> * </span>'); 
						console.log(data);
						}
					});
					
					$.ajax({ 
						type: "POST", 
						url: "load/dropdownsub3.php", 
						data: {stream:stream,sub3:sub3},
						success: function(data){ 
							//$("#txtsubname1").html(''); 
							$('label[id*="txtsubname3"]').text('');
							$("#txtsubname3").html(data+'<span class="required"> * </span>'); 
						console.log(data);
						}
					});
	}	//for change the text of the subject lable for other stream
	else if(stream=="Other")
	{
		$('#txtother').change(function(){

			var stream = $(this).val();
			var sub1 = "othersubject1";
			var sub2 = "othersubject2";
			var sub3 = "othersubject3";
		// var dataString = "stream="+stream+"sub1="+sub1; 

					$.ajax({ 
						type: "POST", 
						url: "load/dropdown.php", 
						data: {stream:stream,sub1:sub1}, 
						success: function(data){ 
							//$("#txtsubname1").html(''); 
							$('label[id*="txtsubname1"]').text('');
							$("#txtsubname1").html(data+'<span class="required"> * </span>'); 
						console.log(data);
						}
					});
					
					$.ajax({ 
						type: "POST", 
						url: "load/dropdownsub2.php", 
						data: {stream:stream,sub2:sub2}, 
						success: function(data){ 
							//$("#txtsubname1").html(''); 
							$('label[id*="txtsubname2"]').text('');
							$("#txtsubname2").html(data+'<span class="required"> * </span>'); 
						console.log(data);
						}
					});
					
					$.ajax({ 
						type: "POST", 
						url: "load/dropdownsub3.php", 
						data: {stream:stream,sub3:sub3}, 
						success: function(data){
							//$("#txtsubname1").html(''); 
							$('label[id*="txtsubname3"]').text('');
							$("#txtsubname3").html(data+'<span class="required"> * </span>'); 
						console.log(data);
						}
					});
				})
	}

});


</script>
