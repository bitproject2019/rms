<?php
include('header.php');
include('side-bar.php');
$username = $_SESSION['username'];
$id = $_SESSION['user_id'];
?>


<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Change Password </div>
					
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Password</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					
					
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Change Password</header>
						<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
							<i class="material-icons">more_vert</i>
						</button>
						<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
							<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
							<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
							<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
						</ul>
					</div>
					<div class="card-body" id="bar-parent">
					<form  id="grade-form" method="post" class="form-horizontal">
										<div class="form-body">
                                            <div class="form-group row" id="txtiddiv" >
                                                <label class="control-label col-md-4" for="txtid">ID
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtid" id="txtid" placeholder="Enter Sub Name" class="form-control input-height" readonly value="<?php echo $id;?>"/>
												</div>
											</div>
                                        
                                        <div class="form-group row">
												<label class="control-label col-md-4" for="txtuser">Username
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="text" name="txtuser" id="txtuser" placeholder="Enter Username Name" class="form-control input-height" readonly value="<?php echo $username;?>"/>
												</div>
                                            
										</div>
										<div class="form-group row">
												<label class="control-label col-md-4" for="txtoldpass">Current Password
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="password" name="txtoldpass" id="txtoldpass" placeholder="Enter Current Password" class="form-control input-height"  />
												</div>
                                            
										</div>
										<div class="form-group row">
												<label class="control-label col-md-4" for="txtpass">New Password
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="password" minlength="6" name="txtpass" id="txtpass" placeholder="Enter New Password" class="form-control input-height"  />
												</div>
                                            
										</div>
										<div class="form-group row">
												<label class="control-label col-md-4" for="txtcpass">Confirm Password
													<span class="required"> * </span>
												</label>
												
                                                <div class="col-md-5">
													<input type="password" name="txtcpass" id="txtcpass" placeholder="Retype the Password" class="form-control input-height"  />
												</div>
                                            
                                        </div>
                                   
										
                                            <div class="form-actions">
												<div class="row">
													<div class="offset-md-5 col-md-9">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<button type="submit"  name="submit_login" id="submit_login" class="btn btn-info m-r-20">Submit</button>
													</div>
												</div>
											</div>
										
										
										</div>
						</form>			
					</div>
				</div>
			</div>
		</div>
	</div>
			</div>


           
<?php
include('footer.php');
?>


<script src="js/jquery.md5.js"></script>

<script>


// for save the data
	function save(){
        //alert('save');
		var result = "changelog";

		var data = $('form').serialize()+'&result='+result;
		$.ajax({

			method:'POST',
			url:"load/update-login.php",
			data:data,
			dataType:"text",
			success:function(data){
				console.log(data);
				$.toast({
					heading: 'User Successfully Updated',
					text: 'Data Updated Successfully',
					position: 'top-right',
					loaderBg:'#ff6849',
					icon: 'success',
					hideAfter: 3500, 

					stack: 6
				});
				
			
			}

		})
		
	}

$(document).ready(function(){
    
   
//for update 
    $('#submit_login').click(function(e){
			
		e.preventDefault();	
		if($('#txtpass').val()==$('#txtcpass').val())
		{
			result='change';
			var data = $('form').serialize()+'&result='+result;
			$.ajax({

				method:'POST',
				url:"load/login-details.php",
				data:data,
				dataType:"json",
				success:function(data){
					
					var old = (data.password);
					var pass = $.md5($('#txtoldpass').val());

					if((pass==old))
					{
						console.log($.md5($('#txtoldpass').val()));
						console.log(old);
						save();
						

					}
					else{
						//console.log('error');
						$.toast({
							heading: 'Check your current password.',
							text: 'Password Error.',
							position: 'top-right',
							loaderBg:'#ff6849',
							icon: 'error',
							hideAfter: 3500
										
						})
						
					}
					
				
				}

			})
			
		}
		else{
			$.toast({
				heading: 'Check your password.',
				text: 'Password Error.',
				position: 'top-right',
				loaderBg:'#ff6849',
				icon: 'error',
				hideAfter: 3500
							
			})
			
		}

    
    });


});
</script>

