<!-- start footer -->
<div class="page-footer">
    <div class="page-footer-inner"> <?php echo date("Y");?> &copy; Vembadi Girls High' School Jaffna. Developed By
        <a href="http://www.infosystm.win" target="blank" class="makerCss">Infosystm</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- end footer -->
</div>

</html>
<!-- start js include path -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/popper/popper.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
<!-- bootstrap -->
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- notifications -->
<script src="assets/plugins/jquery-toast/dist/jquery.toast.min.js"></script>
	<script src="assets/plugins/jquery-toast/dist/toast.js"></script>
<!-- Common js-->
<script src="assets/js/app.js"></script>
<script src="assets/js/layout.js"></script>
<script src="assets/js/theme-color.js"></script>
<!-- Material -->
<script src="assets/plugins/material/material.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/jquery.toast.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/toast.js"></script>
<!-- end js include path -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="assets/plugins/select2/js/select2.js"></script>
<script src="assets/js/pages/select2/select2-init.js"></script>
<script src="assets/yearpicker.js" async></script>
<!-- Data Table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables/export/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/export/buttons.flash.min.js"></script>
<script src="assets/plugins/datatables/export/jszip.min.js"></script>
<script src="assets/plugins/datatables/export/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/export/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/export/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/export/buttons.print.min.js"></script>
<script src="assets/js/pages/table/table_data.js"></script>
  <!--tags input-->
  <script src="assets/plugins/jquery-tags-input/jquery-tags-input.js"></script>
    <script src="assets/plugins/jquery-tags-input/jquery-tags-input-init.js"></script>

    <script src="assets/js/form-script.js"></script>

    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
