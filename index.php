<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="RedstarHospital" />
    <title>Vembadi Girls' High School</title>
        <!-- Jquery Toast css -->
	<link rel="stylesheet" href="assets/plugins/jquery-toast/dist/jquery.toast.min.css">
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- google font -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../assets/plugins/iconic/css/material-design-iconic-font.min.css">
	<!-- bootstrap -->
	<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- style -->
	<link rel="stylesheet" href="../assets/css/pages/extra_pages.css">
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/favicon.ico" />
    <link href="fonts/material-design-icons/material-icon.css" rel="stylesheet" type="text/css">
</head>

<body background="assets/img/bg2.jpg" style=" background-repeat: no-repeat; background-size: cover;">
	<div class="limiter">
        
		<div class="container-login100 page-background2">
            <div class="container">
                <div class="row">
                <div class="col-sm-4">
                    
                </div>
                <div class="col-sm-4">
                    <img src="assets/img/Vembadi.png" alt="">
                </div>
                <div class="col-sm-4">
                
                </div>
            </div>
            </div>
            
            <div class="row">
                <div class="col-sm-3">
                    <a href="login.php">
                        <button class="login100-form-btn btn btn-primary" >
                            Login
                        </button>
                    </a>
                </div>
                <div class="col-sm-3">
                    
                </div>
                <div class="col-sm-3">
                    <a href="signup.php">
                        <button class="login100-form-btn btn btn-primary" >
                            Signup
                        </button>
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                <div class="col-sm-2">
                    
                </div>
                <div class="col-sm-2">
                    <h3>Powered By: - </h3>
                   
                </div>
                <div class="col-sm-8">
                    
                    <h4><i class="material-icons f-left">web</i> <a href="http://www.infosystm.win" target="_blank">Infosystm </a></h4>
                   
                    <h4><i class="material-icons f-left">call</i> <a href="tel:+94777774579"> 0777774579</a></h4>
                    
                    <h4><i class="material-icons f-left">email</i> <a href="mailto:info@infosystm.com">info@infosystm.com</a></h4>
                   
                </div>
            </div>
            </div>
           
		
		</div>
	</div>
	<!-- start js include path -->
	<script src="../assets/plugins/jquery/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/pages/extra-pages/pages.js"></script>
    <script src="assets/plugins/material/material.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/jquery.toast.min.js"></script>
<script src="assets/plugins/jquery-toast/dist/toast.js"></script>
	<!-- end js include path -->
</body>

<script>

// for save the data
	function save(){
		var result = "login";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

            method:'POST',
            url:"load/login.php",
            data:data,
            dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			if(data=="Login Success")
			{
                
				$.toast({
                    heading: 'User Added Successfully',
                    text: 'Wait for getting activated',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
				});
                window.location.assign('home.php');

			}
			else if(data=="please fill proper details")
			{
                $.toast({
                    heading: 'User Account Error',
                    text: 'Check the Account Details',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                    
                });
			}
            console.log(data);
            $('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
     

		});
		
	}
	//for required
	function required(){
        $.toast({
            heading: 'Please Fill The All Details.',
            text: 'All fileds are must.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
                    
        });
	}
	//same value for subject validation

	

$(document).ready(function(){
   // $('input[type=text]').css('text-transform','capitalize');

   
    $( "form" ).on( "submit", function( event ) {
            event.preventDefault();
            //required validation
             if($('#txtusername').val()!=''&& $('#txtpass').val()!='' )
                {
                    save();
                }
                else
                {
                    required();
                }
        

    })
			

})
			
</script>  
</html>