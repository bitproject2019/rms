<?php
include_once('connection.php');

//for all streams details
if(($_POST["result"]=="view"))  
{  
    $output = '';

    $sql = $mysqli->query("select * from stream ");
    $output = '';
    while ($data = $sql->fetch_array()) {
        //other stream with sub category name
       if($data['name']=="Other")
       {
        $output .= ' 
        <tr class="odd gradeX">
            <td class="user-circle-img sorting_1">
                #' . $data['id'] . '
            </td>
            <td class="center">' . $data['name'] . ' | ' .$data['other_stream'].' </td>
            <td class="center">' . $data['subject1'] . '</td>
            <td class="center">' . $data['subject2'] . '</td>
            <td class="center">' . $data['subject3'] . '</td>
            <td class="center">
                <button  type="button" class="btn btn-primary btn-xs edit"  id="'.$data["id"].'" value="'.$data['id'].'">
                <i class="fa fa-pencil"></i>
                </a>
                
            </td>
        </tr>

        ';
       }
       else
       {
        $output .= ' 
        <tr class="odd gradeX">
            <td class="user-circle-img sorting_1">
                #' . $data['id'] . '
            </td>
            <td class="center">' . $data['name'] . '</td>
            <td class="center">' . $data['subject1'] . '</td>
            <td class="center">' . $data['subject2'] . '</td>
            <td class="center">' . $data['subject3'] . '</td>
            <td class="center">
                <button  type="button" class="btn btn-primary btn-xs edit"  id="'.$data["id"].'" value="'.$data['id'].'">
                <i class="fa fa-pencil"></i>
                </a>
                
            </td>   
        </tr>

        ';
       }
    }
    echo $output;
}
//fill update stream model
if(($_POST["result"])=="fetch")  
{  
    $query = "SELECT * FROM stream WHERE id = '".$_POST["id"]."'";  
    $result = mysqli_query($mysqli, $query);  
    $row = mysqli_fetch_array($result);  
    echo json_encode($row);  
}  

//===================================================================================

// for aall grade details or division details
if(($_POST["result"]=="gradesview"))  
{  
    $output = '';

    $sql = $mysqli->query("select * from grade ");
    $output = '';
    while ($data = $sql->fetch_array()) {
        //other stream with sub category name
        if($data['stream']=="Other")
        {
            $output .= ' 
            <tr class="odd gradeX">
                <td class="user-circle-img sorting_1">
                    ' . $data['stream']. ' | '.$data['other_stream'].'
                </td>
                <td class="center">' . $data['division'] . '</td>
                <td class="center">' . $data['medium'] . '</td>
             
                <td class="center">
                    <button  type="button" class="btn btn-primary btn-xs edit"  id="'.$data["id"].'" value="'.$data['id'].'">
                    <i class="fa fa-pencil"></i>
                    </a>
                    
                </td>
            </tr>

            ';
        }
        else
        {
            $output .= ' 
            <tr class="odd gradeX">
                <td class="user-circle-img sorting_1">
                    ' . $data['stream'] . '
                </td>
                <td class="center">' . $data['division'] . '</td>
                <td class="center">' . $data['medium'] . '</td>
             
                <td class="center">
                    <button  type="button" class="btn btn-primary btn-xs edit"  id="'.$data["id"].'" value="'.$data['id'].'">
                    <i class="fa fa-pencil"></i>
                    </a>
                    
                </td>               
            </tr>

            ';
        }
       
    }
    echo $output;
}

//fill update grade model
if(($_POST["result"])=="fetchgrade")  
{  
    $query = "SELECT * FROM grade WHERE id = '".$_POST["id"]."'";  
    $result = mysqli_query($mysqli, $query);  
    $row = mysqli_fetch_array($result);  
    echo json_encode($row);  
}  
//======================================================================================
// for subject view
if(($_POST["result"]=="viewsubject"))  
{  
    $output = '';

    $sql = $mysqli->query("select * from subject ");
    $output = '';
    while ($data = $sql->fetch_array()) {
        //other stream with sub category name
        $tamil = ($data['tamil']!="" && $data['english']!="") ? $data['english'].' & '.$data['tamil']:$data['english'].$data['tamil'];
        $output .= ' 
        <tr class="odd gradeX">
            <td class="user-circle-img sorting_1">
                ' . $data['name'] . '
            </td>

            <td>' .$tamil.'</td>

            <td class="center">
                <button  type="button" class="btn btn-primary btn-xs edit"  id="'.$data["id"].'" value="'.$data['id'].'">
                <i class="fa fa-pencil"></i>
                </a>
                
            </td>

        </tr>

        ';
        
       
    }
    echo $output;
}

//fill update subject model
if(($_POST["result"])=="fetchsubject")  
{  
    $query = "SELECT * FROM subject WHERE id = '".$_POST["id"]."'";  
    $result = mysqli_query($mysqli, $query);  
    $row = mysqli_fetch_array($result);  
    echo json_encode($row);  
} 
?>
	
  

 
