<?php
include_once('connection.php');

//count all student
$output = '';
$bio = '';
$maths = '';
$arts = '';
$commerce = '';
$engineering = '';
$biotech = '';
$other = '';
$bioper = '';
$mathsper = '';
$artsper = '';
$commerceper = '';
$engineeringper = '';
$biotechper = '';
$otherper = '';
$date = date("Y");
$sql = $mysqli->query("select count(id) as student from students where year ='$date' ");
$sql2 = $mysqli->query("select count(id) as bio from students where stream='Bio' and year ='$date' ");
$sql3 = $mysqli->query("select count(id) as maths from students where stream='Maths' and year ='$date'  ");
$sql4 = $mysqli->query("select count(id) as commerce from students where stream='Commerce'  and year ='$date' ");
$sql5 = $mysqli->query("select count(id) as arts from students where stream='Arts' and year ='$date'  ");
$sql6 = $mysqli->query("select count(id) as engineering from students where stream='Eng-Technology' and year ='$date' ");
$sql7 = $mysqli->query("select count(id) as biotech from students where stream='Bio-Technology' and year ='$date' ");
$sql8 = $mysqli->query("select count(id) as other from students where stream='Other' and year ='$date' ");
while ($data = $sql->fetch_array() ) {
   
  
    $output .=  $data['student'] ;
  
}
while ($data = $sql2->fetch_array() ) {
       
      
    $bio .=  $data['bio'] ;
    $bioper .=  round(($bio /$output) *100,2);
  
}
while ($data = $sql3->fetch_array() ) {
       
      
    $maths .=  $data['maths'] ;
    $mathsper .=  round(($maths /$output) *100,2);
  
}
while ($data = $sql4->fetch_array() ) {
       
      
    $commerce .=  $data['commerce'] ;
    $commerceper .=  round(($commerce/$output) *100,2);
  
}
while ($data = $sql5->fetch_array() ) {
       
      
    $arts .=  $data['arts'] ;
    $artsper .=  round(($arts/$output) *100,2);
  
}
while ($data = $sql6->fetch_array() ) {
       
      
    $engineering .=  $data['engineering'] ;
    $engineeringper .=  round(($engineering/$output) *100,2);
  
}
while ($data = $sql7->fetch_array() ) {
       
      
    $biotech .=  $data['biotech'] ;
    $biotechper .=  round(($biotech/$output) *100,2);
  
}
while ($data = $sql8->fetch_array() ) {
       
      
    $other .=  $data['other'] ;
    $otherper .=  round(($other/$output) *100,2);
  
}
$arr = array(
    "tot"=>$output,
    "bio"=>$bio,
    "percent"=>$bioper,
    "maths"=>$maths,
    "mathspercent"=>$mathsper,
    "commerce"=>$commerce,
    "commercepercent"=>$commerceper,
    "arts"=>$arts,
    "artspercent"=>$artsper,
    "engineering"=>$engineering,
    "engineeringpercent"=>$engineeringper,
    "biotech"=>$biotech,
    "biotechpercent"=>$biotechper,
    "other"=>$other,
    "otherpercent"=>$otherper,
    );
echo json_encode($arr);


?>