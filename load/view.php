<?php
$date = date("Y");
include_once('connection.php');
$subject1 = '';
$subject2 = '';
$subject3 = '';
$stream_name = $_POST['active_path'];

    $sql = $mysqli->query("SELECT subject1,subject2,subject3 FROM stream where name = '$stream_name'");
    while ($data = $sql->fetch_array()) {
        $subject1 = $data['subject1'];
        $subject2 = $data['subject2'];
        $subject3 = $data['subject3'];
    }

if($_POST['active_path'] == 'Bio'){

    $sql = $mysqli->query("SELECT results.index_no,students.fullname,students.grade,results.islandrank,results.rank,
    MAX(CASE WHEN subject_id = '$subject1' THEN result END) `$subject1`,
    MAX(CASE WHEN subject_id = '$subject2' THEN result END) `$subject2`,
    MAX(CASE WHEN subject_id = '$subject3' THEN result END) `$subject3`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Bio' AND results.year = '$date'
    GROUP BY index_no");
    
    $output ='';
    $head ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    while ($data = $sql->fetch_array()) {
        $output .= ' 
            <tr>
                <td style="white-space: inherit">' . $data['index_no'] . '</td>
                <td style="white-space: inherit">' . $data['fullname'] . '</td>
                <td style="white-space: inherit">' . $data['grade'] . '</td>
                <td style="white-space: inherit">' . $data[$subject1] . '</td>
                <td style="white-space: inherit">' . $data[$subject2] . '</td>
                <td style="white-space: inherit">' . $data[$subject3] . '</td>
                <td style="white-space: inherit">' . $data['rank'] . '</td>
                <td style="white-space: inherit">' . $data['islandrank'] . '</td>
                <td >
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1"     id="' . $data['index_no'] . '" >
                    <i class="fa fa-pencil"></i>
                    </button>
                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>
        ';
    }
    $foot ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">Rank</th>
            <th style="white-space: inherit">Island rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    $returnArr = [$head,$output,$foot];    
    
    echo json_encode($returnArr);
    //echo $output;
    
}
else if($_POST['active_path'] == 'Maths'){
    $sql = $mysqli->query("SELECT results.index_no,students.fullname,students.grade,results.islandrank,results.rank,
    MAX(CASE WHEN subject_id = '$subject1' THEN result END) `$subject1`,
    MAX(CASE WHEN subject_id = '$subject2' THEN result END) `$subject2`,
    MAX(CASE WHEN subject_id = '$subject3' THEN result END) `$subject3`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Maths' AND results.year = '$date'
    GROUP BY index_no");
    $output ='';
    $head ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    while ($data = $sql->fetch_array()) {
        $output .= ' 
            <tr>
                <td>' . $data['index_no'] . '</td>
                <td style="white-space: inherit">' . $data['fullname'] . '</td>
                <td>' . $data['grade'] . '</td>
                <td>' . $data[$subject1] . '</td>
                <td>' . $data[$subject2] . '</td>
                <td>' . $data[$subject3] . '</td>
                <td style="white-space: inherit">' . $data['rank'] . '</td>
                <td style="white-space: inherit">' . $data['islandrank'] . '</td>
                <td style="white-space: inherit">
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1"     id="' . $data['index_no'] . '" >
                    <i class="fa fa-pencil"></i>
                    </button>

                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>
        ';
    }
    $foot ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">GK</th>
            <th style="white-space: inherit">English</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    $returnArr = [$head,$output,$foot];    
    echo json_encode($returnArr);
}
else if($_POST['active_path'] == 'Arts'){
    $sql = $mysqli->query("SELECT results.index_no,students.fullname,students.grade,results.islandrank,results.rank,
    MAX(CASE WHEN subject_id = '$subject1' THEN result END) `$subject1`,
    MAX(CASE WHEN subject_id = '$subject2' THEN result END) `$subject2`,
    MAX(CASE WHEN subject_id = '$subject3' THEN result END) `$subject3`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Arts' AND results.year = '$date'
    GROUP BY index_no");
    $output ='';
    $head ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    while ($data = $sql->fetch_array()) {
        $output .= ' 
            <tr>
                <td>' . $data['index_no'] . '</td>
                <td style="white-space: inherit">' . $data['fullname'] . '</td>
                <td>' . $data['grade'] . '</td>
                <td>' . $data[$subject1] . '</td>
                <td>' . $data[$subject2] . '</td>
                <td>' . $data[$subject3] . '</td>
                <td style="white-space: inherit">' . $data['rank'] . '</td>
                <td style="white-space: inherit">' . $data['islandrank'] . '</td>
                <td style="white-space: inherit">
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1"     id="' . $data['index_no'] . '" >
                    <i class="fa fa-pencil"></i>
                    </button>

                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>
        ';
    }
    $foot ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    $returnArr = [$head,$output];    
    echo json_encode($returnArr);
}
else if($_POST['active_path'] == 'Commerce'){
    $sql = $mysqli->query("SELECT results.index_no,students.fullname,students.grade,results.islandrank,results.rank,
    MAX(CASE WHEN subject_id = '$subject1' THEN result END) `$subject1`,
    MAX(CASE WHEN subject_id = '$subject2' THEN result END) `$subject2`,
    MAX(CASE WHEN subject_id = '$subject3' THEN result END) `$subject3`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Commerce' AND results.year = '$date'
    GROUP BY index_no");
    $output ='';
    $head ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    while ($data = $sql->fetch_array()) {
        $output .= ' 
            <tr>
                <td>' . $data['index_no'] . '</td>
                <td style="white-space: inherit">' . $data['fullname'] . '</td>
                <td>' . $data['grade'] . '</td>
                <td>' . $data[$subject1] . '</td>
                <td>' . $data[$subject2] . '</td>
                <td>' . $data[$subject3] . '</td>
                <td style="white-space: inherit">' . $data['rank'] . '</td>
                <td style="white-space: inherit">' . $data['islandrank'] . '</td>
                <td style="white-space: inherit">
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1"     id="' . $data['index_no'] . '" >
                    <i class="fa fa-pencil"></i>
                    </button>

                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>
        ';
    }
    $foot ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    $returnArr = [$head,$output];    
    echo json_encode($returnArr);
}
else if($_POST['active_path'] == 'Eng-Technology'){
    $sql = $mysqli->query("SELECT results.index_no,students.fullname,students.grade,results.islandrank,results.rank,
    MAX(CASE WHEN subject_id = '$subject1' THEN result END) `$subject1`,
    MAX(CASE WHEN subject_id = '$subject2' THEN result END) `$subject2`,
    MAX(CASE WHEN subject_id = '$subject3' THEN result END) `$subject3`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Eng-Technology' AND results.year = '$date'
    GROUP BY index_no");
    $output ='';
    $head ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    while ($data = $sql->fetch_array()) {
        $output .= ' 
            <tr>
                <td style="white-space: inherit">' . $data['index_no'] . '</td>
                <td style="white-space: inherit">' . $data['fullname'] . '</td>
                <td style="white-space: inherit">' . $data['grade'] . '</td>
                <td style="white-space: inherit">' . $data[$subject1] . '</td>
                <td style="white-space: inherit">' . $data[$subject2] . '</td>
                <td style="white-space: inherit">' . $data[$subject3] . '</td>
                <td style="white-space: inherit">' . $data['rank'] . '</td>
                <td style="white-space: inherit">' . $data['islandrank'] . '</td>
                <td style="white-space: inherit">
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1"     id="' . $data['index_no'] . '" >
                    <i class="fa fa-pencil"></i>
                    </button>
                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>
        ';
    }
    $foot ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    $returnArr = [$head,$output,$foot];    
    echo json_encode($returnArr);
}
else if($_POST['active_path'] == 'Bio-Technology'){
    $sql = $mysqli->query("SELECT results.index_no,students.fullname,students.grade,results.islandrank,results.rank,
    MAX(CASE WHEN subject_id = '$subject1' THEN result END) `$subject1`,
    MAX(CASE WHEN subject_id = '$subject2' THEN result END) `$subject2`,
    MAX(CASE WHEN subject_id = '$subject3' THEN result END) `$subject3`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Bio-Technology' AND results.year = '$date'
    GROUP BY index_no");
    $output ='';
    $head ='
        <tr>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    while ($data = $sql->fetch_array()) {
        $output .= ' 
            <tr>
                 <td style="white-space: inherit">' . $data['index_no'] . '</td>
                <td style="white-space: inherit">' . $data['fullname'] . '</td>
                <td style="white-space: inherit">' . $data['grade'] . '</td>
                <td style="white-space: inherit">' . $data[$subject1] . '</td>
                <td style="white-space: inherit">' . $data[$subject2] . '</td>
                <td style="white-space: inherit">' . $data[$subject3] . '</td>
                <td style="white-space: inherit">' . $data['rank'] . '</td>
                <td style="white-space: inherit">' . $data['islandrank'] . '</td>
                <td style="white-space: inherit">
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1"     id="' . $data['index_no'] . '" >
                    <i class="fa fa-pencil"></i>
                    </button>
                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>
        ';
    }
    $foot ='
        <tr>
            <th>Index<br>No</th>
            <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">'.$subject1.'</th>
            <th style="white-space: inherit">'.$subject2.'</th>
            <th style="white-space: inherit">'.$subject3.'</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    $returnArr = [$head,$output,$foot];    
    echo json_encode($returnArr);
}
else if($_POST['active_path'] == 'Other'){
    $result = mysqli_query($mysqli,"SELECT other_stream FROM `students` WHERE students.stream = 'Other'");
    $row = mysqli_fetch_array($result);
    $other_stream = $row['other_stream'];
    $subject1= '';
    $subject2= '';
    $subject3= '';
    $sql = $mysqli->query("SELECT subject1,subject2,subject3 FROM stream where other_stream = '$other_stream'");
    while ($data = $sql->fetch_array()) {
        $other_subject1 = $data['subject1'];
        $other_subject2 = $data['subject2'];
        $other_subject3 = $data['subject3'];
    }
    
    $sql = $mysqli->query("SELECT results.index_no,results.islandrank,students.fullname,students.grade,results.rank,
    MAX(CASE WHEN subject_id = '$other_subject1' THEN result END) `$other_subject1`,
    MAX(CASE WHEN subject_id = '$other_subject2' THEN result END) `$other_subject2`,
    MAX(CASE WHEN subject_id = '$other_subject3' THEN result END) `$other_subject3`,
   
    MAX(CASE WHEN subject_id = 'GK' THEN result END) `GK`,
    MAX(CASE WHEN subject_id = 'General English' THEN result END) `General English`
    FROM results 
    INNER JOIN students ON results.index_no = students.index_no
    WHERE students.stream = 'Other'
    GROUP BY index_no");
   
    $output ='';
    $head ='
        <tr>
             <th style="white-space: inherit">Index<br>No</th>
            <th style="white-space: inherit">Name</th>
            <th style="white-space: inherit">Division</th>
            <th style="white-space: inherit">GK</th>
            <th style="white-space: inherit">General English</th>
            <th style="white-space: inherit">District Rank</th>
            <th style="white-space: inherit">Island Rank</th>
            <th style="white-space: inherit">Action</th>
        </tr>
    ';
    
    
    while ($data = $sql->fetch_array() ) {
        $output .= ' 
            <tr>

                <td>' . $data['index_no'] . '</td>
                <td>' . $data['fullname'] . '</td>
                <td>' . $data['grade'] . '</td>
                <td>' . $data['GK'] . '</td>
                <td>' .$data['General English'] . '</td>
                <td>'. $data['rank'] . '</td>
                <td>'. $data['islandrank'] . '</td>
                <td>
                    <button class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#myModal1" id="' . $data['index_no'] . '" >
                        <i class="fa fa-pencil"></i>
                    </button>
                   
                    <button class="btn btn-primary btn-xs view" data-toggle="modal" data-target="#myModal2"   id="' . $data['index_no'] . '">
                        <i class="fa fa-eye"></i>
                    </button>
                   
                    
                </td>

            </tr>
        ';
    }
       
    $returnArr = [$head,$output];    
    echo json_encode($returnArr);
}


