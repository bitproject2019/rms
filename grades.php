<?php
include('header.php');
include('side-bar.php');

?>
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">All Divisions</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Divisions</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">All Divisions</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>All Divisions</header>
		
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body " id="bar-parent">
									<table id="exportTable1" class="display nowrap" style="width:100%">
										<thead>
											<tr>
												<th>Stream</th>
												<th>Division</th>
												<th>Medium</th>
												<th>Action</th>
										
											</tr>
										</thead>
										<tbody id="tbody">

										</tbody>
										<tfoot>
											<tr>
                                                <th>Stream</th>
												<th>Division</th>
												<th>Medium</th>
												<th>Action</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="container">	
	<div class="modal fade" id="myModal" role="dialog">
   		<div class="modal-dialog modal-lg">
      		<div class="modal-content">
     
       			 <div class="modal-body">
        			<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Grade Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
								</div>
								<div class="card-body" id="bar-parent">
									<form  id="grade-form" method="post" class="form-horizontal">
										<div class="form-body">
                                        <div class="form-group row" id="txtiddiv" hidden >
												
												<div class="col-md-5">
													<input type="text" name="txtid" id="txtid" placeholder="Enter Sub Name" class="form-control input-height" />
												</div>
											</div>
                      <div class="form-group row">
												<label class="control-label col-md-4" for="txtstream">Stream
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<select class="form-control input-height" name="txtstream" id="txtstream">
														<option value="">Select...</option>
														<option value="Bio">Bio</option>
														<option value="Maths">Maths</option>
														<option value="Commerce">Commerce</option>
														<option value="Arts">Arts</option>
														<option value="Eng-Technology">Eng-Technology</option>
														<option value="Bio-Technology">Bio-Technology</option>
														<option value="Other">Others</option>

                          </select>
												</div>
											</div>
											<div class="form-group row" id="divother" hidden>
												<label class="control-label col-md-4" for="txtother">Other Stream Name
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<select class="form-control input-height" name="txtother" id="txtother">
													<?php
												
												include_once('load/connection.php');
												//$output = '';
												$sql = $mysqli->query("SELECT * FROM stream where other_stream!=''");
												$output = '<option value="">Select...</option>';
												while ($row = $sql->fetch_array()) {
													$output .= '<option value="'.$row["other_stream"].'">'. $row["other_stream"] .'</option>';
												}
												//AND travelers_room_status.room_type='" . $_POST['room'] . "'
												echo $output;
											?>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-md-4" for="txtdivision">Division
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<input type="text" name="txtdivision" id="txtdivision" placeholder="Enter Division" class="form-control input-height" maxlength="1" />
												</div>
											</div>
											
											<div class="form-group row">
												<label class="control-label col-md-4" for="txtmedium">Medium
													<span class="required"> * </span>
												</label>
												<div class="col-md-5">
													<select class="form-control input-height" name="txtmedium" id="txtmedium" >
														<option value="">Select...</option>
														<option value="Tamil">Tamil</option>
														<option value="English">English</option>
													</select>
												</div>
                                            </div>
                        <div class="form-actions">
												<div class="row">
													<div class="offset-md-3 col-md-9">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<button type="submit"  name="submit_grade" id="submit_grade" class="btn btn-info m-r-20">Submit</button>
													</div>
												</div>
											</div>
										</div>
									</form>								</div>
							</div>
						</div>
					</div>
				</div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
</div> 

<?php

    include('footer.php');
?>

<script>


// for save the data
	function save(){
        //alert('save');
		var result = "grade";

		var data = $('form').serialize()+"&result="+result;
		$.ajax({

			method:'POST',
			url:"load/update.php",
			data:data,
			dataType:"text",
			success:function(data){
				console.log(data);
				$.toast({
					heading: 'Grade Successfully Updated',
					text: 'Data Updated Successfully',
					position: 'top-right',
					loaderBg:'#ff6849',
					icon: 'success',
					hideAfter: 3500, 

					stack: 6
				});
				//window.setTimeout(function(){location.reload()},1000);
				load();
				$('#myModal').modal('hide');
				hiddenRemove();
			}

		})
		
	}
	
	//same value for subject validation
	function samevalue(){
        $.toast({
            heading: 'Same Value for Subjects.',
            text: 'Please Select Different Subjects for Every Field.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
                    
        });
	}
function load(){
	var result = "gradesview";
    $.ajax({

        method:'POST',
        url:"load/viewstream.php",
        data:{result:result},
        dataType:"text",
        success:function(res){
            $('#tbody').html(res);
           

        }

    });
}
$(document).ready(function(){
    //load devisions details from view page
	$('input[type=text]').css('text-transform','capitalize'); // capitalize case
//	dataTable();
	load();
	var result = "gradesview";
    $.ajax({

        method:'POST',
        url:"load/viewstream.php",
        data:{result:result},
        dataType:"text",
        success:function(res){
            $('#tbody').html(res);
			dataTable();

        }

    });
//for update 
    $('#submit_grade').click(function(e){
			
           e.preventDefault();	
		if($('#txtstream').val() =="Other")
		{
			if($('#txtstream').val() =="" || $('#txtmedium').val()=="" || $('#txtdivision').val()=="" || $('#txtother').val()=="" )
			{
				required();
			}
			else
			{
				save();
			
			}
		}
		else{

			if($('#txtstream').val() =="" || $('#txtmedium').val()=="" || $('#txtdivision').val()=="" )
			{
				required();
			}
			else
			{
				// console.log('dsdsdsdsd');
				save();
				
			}
		}
    })

        //for other stream load
        $('#txtstream').change(function(){
			hiddenRemove();
		})
	

});

// open model
$(document).on('click', '.edit', function(){  
	var result = "fetchgrade";
    var id = $(this).attr("id");  
    $('#myModal').modal('show');
	//$(".modal-body").html($(".row").find('.card-box').html());
    $.ajax({  
        url:"load/viewstream.php",  
        method:"POST",  
        data:{id:id,result:result},  
        dataType:"json",  
        success:function(data){  
            $('#txtid').val(data.id);  
            $('#txtstream').val(data.stream);  
            $('#txtdivision').val(data.division);  
            $('#txtmedium').val(data.medium);  
            $('#txtother').val(data.other_stream);  

            if(data.stream=="Other")
            {
                hiddenRemove();
            }
            else{
                $('#divother').attr("hidden","true");

            }
                
        }  
    });  
}); 


</script>