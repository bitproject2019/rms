<?php
include('header.php');
include('side-bar.php');
$active_path = $_SERVER[ 'QUERY_STRING' ]; 
?>
<style>
.tableedit{
	text-align: center;
	margin: 20px 0 !important;
}
</style>
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Students Results</div>
					<!-- Button trigger modal -->

				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Results</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">All Results</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel tab-border card-box">
					<header class="panel-heading panel-heading-gray custom-tab ">
						<ul class="nav nav-tabs">
							<li class="nav-item url"><a href="#Bio" data-toggle="tab" 
							class="<?php echo($active_path == 'Bio'?'active' :'' )?>">Bio</a>
							</li>
							<li class="nav-item url"><a href="#Maths" class="<?php echo($active_path == 'Maths'?'active' :'' )?>" data-toggle="tab">Maths</a>
							</li>
							<li class="nav-item url"><a href="#Arts" class="<?php echo($active_path == 'Arts'?'active' :'' )?>"  data-toggle="tab">Arts</a>
							</li>
							<li class="nav-item url"><a href="#Commerce" class="<?php echo($active_path == 'Commerce'?'active' :'' )?> " data-toggle="tab">Commerce</a>
							</li>
							<li class="nav-item url"><a href="#Eng-Technology" class="<?php echo($active_path == 'Eng-Technology'?'active' :'' )?>" data-toggle="tab">Eng-Technology</a>
							</li>
							<li class="nav-item url"><a href="#Bio-Technology" class="<?php echo($active_path == 'Bio-Technology'?'active' :'' )?>"data-toggle="tab">Bio-Technology</a>
							</li>
							<li class="nav-item url"><a href="#Other" class="<?php echo($active_path == 'Other'?'active' :'' )?>" data-toggle="tab">Other Stream</a>
							</li>
						</ul>
					</header>
					<div class="panel-body">
						<div class="tab-content">
							<div class="<?php echo($active_path == 'Bio'?'tab-pane active' :'tab-pane' )?>" id="Bio">
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<div class="card card-box">
											<div class="card-body " id="bar-parent">
											<table class="display nowrap" id="exportTable1" style="width:100%">
													<thead class="head">
														
													</thead>
													<tbody class="datas">
														
														
													</tbody>
													<tfoot class="head">
														
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>		
							</div>
		
							<div class="<?php echo($active_path == 'Maths'?'tab-pane active' :'tab-pane' )?>" id="Maths">
							<div class="row">
									<div class="col-md-12 col-sm-12">
										<div class="card card-box">
											<div class="card-body " >
												<table  class="display nowrap" id="exportTable2" style="width:100%">
													<thead class="head">
															
													</thead>
													<tbody class="datas">
															
															
													</tbody>
													<tfoot class="head">
														
													</tfoot>
														
												</table>
											</div>
										</div>
									</div>
								</div>	
		
							</div>
					
					<div class="<?php echo($active_path == 'Arts'?'tab-pane active' :'tab-pane' )?>" id="Arts">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="card card-box">
									<div class="card-body " >
										<table class="display nowrap" id="exportTable3" style="width:100%">
											<thead class="head">
													
											</thead>
											<tbody class="datas">
													
													
											</tbody>
											<tfoot class="head">
												
											</tfoot>
														
										</table>
									
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="<?php echo($active_path == 'Commerce'? 'tab-pane active' :'tab-pane' )?>"  id="Commerce">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="card card-box">
									<div class="card-body">
										<table class="display nowrap" id="exportTable4" style="width:100%" id="exportTable3">
											<thead class="head">
														
											</thead>
											<tbody class="datas">
														
														
											</tbody>
											<tfoot class="head">
													
											</tfoot>
														
											</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="<?php echo($active_path == 'Eng-Technology'?'tab-pane active' :'tab-pane' )?>" id="Eng-Technology">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="card card-box">
									<div class="card-body " >
										<table class="display nowrap" id="exportTable5" style="width:100%">
											<thead class="head">
													
											</thead>
											<tbody class="datas">
													
													
											</tbody>
											<tfoot class="head">
												
											</tfoot>
												
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="<?php echo($active_path == 'Bio-Technology'?'tab-pane active' :'tab-pane' )?>" id="Bio-Technology">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="card card-box">
									<div class="card-body " >
										<table class="display nowrap dataTable" id="exportTable6" style="width:100%">
											<thead class="head">
													
											</thead>
											<tbody class="datas">
													
													
											</tbody>
											<tfoot class="head">
												
											</tfoot>
												
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="<?php echo($active_path == 'Other'?'tab-pane active' :'tab-pane' )?>" id="Other">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="card card-box">
									<div class="card-body " >
										<table class="display nowrap dataTable" id="exportTable7" style="width:100%">
											<thead class="head">
													
											</thead>
											<tbody class="datas">
													
													
											</tbody>
											<tfoot class="head">
												
											</tfoot>
												
										</table>
									</div>
								</div>
							</div>
						</div>
						
					</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<!-- end page content -->

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title" id="myModalLabel">Results Update</h4>
				<h4 data-dismiss="modal" class="btn btn-default"  >X</h4>
			</div>
      
	  
    	<div class="modal-body" style="width: 644px;">
			<form  id="update-results" class="form-horizontal" method="post" >
	  			<div class="form-body">
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtregno">Reg No
							<span class="required"> </span>
						</label>
						<div class="col-md-5">
							<input type="text" name="txtregno" id="txtregno" placeholder="Enter Reg No" class="form-control input-height" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtfullname">Full Name
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input type="text" name="txtfullname" id="txtfullname" placeholder="Enter Fullname" class="form-control input-height" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtindex">Index No
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input type="text" name="txtindex" id="txtindex" placeholder="Enter Index" class="form-control input-height" /> </div>
					</div>
					
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtyear">Year
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input type="text" id="txtyear" name="txtyear" placeholder="Select Year" class="form-control yearpicker input-height" /> 
						</div>
						
					</div>
					
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtsitting">Sitting
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<select class="form-control input-height"  id="txtsitting" name="txtsitting">
											<option value="First">First </option>
											<option value="Second">Second</option>
											<option value="Third">Third</option>
					
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtstream">Stream
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<select class="form-control input-height" name="txtstream" id="txtstream">
								<option value="">Select...</option>
								<option value="Bio">Bio</option>
								<option value="Maths">Maths</option>
								<option value="Arts">Arts</option>
								<option value="Commerce">Commerce</option>
								<option value="Eng-Technology">Eng-Technology</option>
								<option value="Bio-Technology">Bio-Technology</option>
								<option value="Other">Other</option>
							</select>
						</div>
					</div>
					<div class="form-group row" id="divother" hidden>
						<label class="control-label col-md-3" for="txtother">Other Stream
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<select class="form-control input-height" name="txtother" id="txtother">
							<?php
						
						include_once('load/connection.php');
						//$output = '';
						$sql = $mysqli->query("SELECT * FROM stream where other_stream!=''");
						$output = '<option value="">Select...</option>';
						while ($row = $sql->fetch_array()) {
							$output .= '<option value="'.$row["other_stream"].'">'. $row["other_stream"] .'</option>';
						}
						//AND travelers_room_status.room_type='" . $_POST['room'] . "'
						echo $output;
					?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtmedium">Medium
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<select class="form-control input-height" name="txtmedium" id="txtmedium">
								<option value="">Select...</option>
								<option value="Tamil">Tamil</option>
								<option value="English">English</option>
							</select>
						</div>
						</div>
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtgrade">Division
							<span class="required">  </span>
						</label>
						<div class="col-md-5">
							<select class="form-control input-height"  id="txtgrade" name="txtgrade">
							<option value="">Select...</option>

							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3" for="txtsub1" id="txtsubname1" name="txtsubname1">			
						Subject 1
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input name="txtsub1" id="txtsub1" type="text" placeholder="Enter Result" class="form-control input-height"  maxlength="1" />
						</div>
																</div>
														<div class="form-group row">
						<label class="control-label col-md-3" for="txtsub2" id="txtsubname2" name="txtsubname2">Subject 2
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input name="txtsub2" id="txtsub2" type="text" placeholder="Enter Result" class="form-control input-height" maxlength="1" />
						</div>
																</div>
																<div class="form-group row">
						<label class="control-label col-md-3" for="txtsub3" id="txtsubname3" name="txtsubname3">Subject 3
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input name="txtsub3" id="txtsub3" type="text" placeholder="Enter Result" class="form-control input-height" maxlength="1"/>
						</div>
																</div>
																<div class="form-group row">
						<label class="control-label col-md-3" for="txtgk" id="txtsubname4" name="txtsubname4">GK
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input name="txtgk" id="txtgk" type="text" placeholder="Enter Result" class="form-control input-height" maxlength="3" />
						</div>
																</div>
																<div class="form-group row">
						<label class="control-label col-md-3" for="txtenglish" id="txtsubname5" name="txtsubname5" >General English
							<span class="required"> * </span>
						</label>
						<div class="col-md-5">
							<input name="txtenglish" id="txtenglish" type="text" placeholder="Enter Result" class="form-control input-height" maxlength="1" />
						</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-3" for="txtrank" >District Rank
						<span class="required"> * </span>
					</label>
					<div class="col-md-5">
						<input name="txtrank" id="txtrank" type="text" placeholder="Enter District Rank" class="form-control input-height num"  />
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-3" for="txtirank" >Island Rank
						<span class="required"> * </span>
					</label>
					<div class="col-md-5">
						<input name="txtirank" id="txtirank" type="text" placeholder="Enter Island Rank" class="form-control input-height num"  />
					</div>
				</div>
      </div>
      <div class="modal-footer" style="width: 385px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="update">Update changes</button>
      </div>
        </form>
    </div>
  </div>
</div>
</div>
  


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title" id="myModalLabel">Results Views</h4>
				<h4 data-dismiss="modal" class="btn btn-default"  >X</h4>
      </div>
      
      <div class="modal-body">
			<div class="table-scrollable">
												<table class="table table-striped table-hover">
													<thead class="head2">
														<tr>
															<th> Subject </th>
															<th> Results </th>
															
														</tr>
													</thead>	
													<tbody class="body2">
													</tbody>
												</table>
											</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
        </form>
    </div>
  </div>
</div>
<?php
include('footer.php');
?>
<script type="text/javascript">


$(document).ready(function(){
	// var url = window.location.href.split('?');
	// var cuurenturl = url[0];
	// var part = cuurenturl.substring(cuurenturl.lastIndexOf("/")+1);
	// // var ids = get_id.substring(1); 
	// // 	var pathArray = window.location.href.split('?');
	// // 	lastpath= pathArray[0];
	// // 	lastpath += '?'+ids
	// // 	window.location.href = lastpath;
	// 	console.log(part);
		

	$("#signin").on( "click", function() {
        $('#myModal1').modal('hide');  
	});
	//trigger next modal
	$("#signin").on( "click", function() {
					$('#myModal2').modal('show');  
	});
	

	$('#update').click(function (e) { 
		e.preventDefault();
		var update_datas = $('#update-results').serialize();
		$.ajax({  
			url:"load/update.php",
			method:"POST", 	
			data:{update_datas:update_datas},  
			dataType:"text",  
			success:function(data){
				//console.log(data);
				
			}  
		});
	});
	//Edit code
	$(document).on('click', '.edit', function(){  
		var result_id = $(this).attr("id"); 
		$.ajax({  
			url:"load/updates.php",
			method:"POST",  
			data:{result_id:result_id},  
			dataType:"json",  
			success:function(data){
				
				$('#txtregno').val(data.registration_no);
				$('#txtfullname').val(data.fullname);
				$('#txtindex').val(data.index_no);
				$('#txtyear').val(data.year);
				$('#txtsitting').val(data.sitting);
				$('#txtstream').val(data.stream);
				$('#txtmedium').val(data.medium);
				$('#txtgrade').find('option').remove().end().append($("<option selected></option>").attr("value",data.grade).text(data.grade)); 
				$('#txtsubname1').text(data.subject1);
				$('#txtsubname2').text(data.subject2);
				$('#txtsubname3').text(data.subject3);
				$('#txtsub1').val(data.result1);
				$('#txtsub2').val(data.result2);
				$('#txtsub3').val(data.result3);
				$('#txtgk').val(data.result4);
				$('#txtenglish').val(data.result5);
				$('#txtrank').val(data.result6);
				$('#txtirank').val(data.result7);
			}  
		});
		  
	}); 
	//data table show 
	var active_path = window.location.search.substring(1);

	if(active_path != 'Other')
	{
		$.ajax({
			method:'POST',
			url:"load/view.php",
			data:{active_path:active_path},
			dataType:'json',
			success: function(data) {
				console.log(data);
				$('.head').html(data[0]);
				$('.datas').html(data[1]);
				dataTable();
			}
		});
	}
	else
	{

		$.ajax({
			method:'POST', 
			url:"load/view.php", 
			data:{active_path:active_path},
			dataType:'json',
			success: function(data) {
				// alert(data);
				console.log(data);
				$('.head').html(data[0]);
				$('.datas').html(data[1]);
				dataTable();
			}
		});
		
		
	}
	$(document).on('click', '.view', function(){  
			
			var indexno = $(this).attr("id"); 
			$.ajax({
				method:'POST', 
				url:"load/othersresult.php", 
				data:{indexno:indexno},
				dataType:'json',
				success: function(data) {
				
					console.log(data);
					
					$('.body2').html(data[0]);
					//dataTable();
				}
			});
		});
	// $(document).on('click','#grade',function () {
	// 	var test2 = $(this).attr('a')
	// 	 var test = window.location.href.split('?')[0];
		 
	// 	alert(test);
	// })
	$('.url').click(function (e) { 
		e.preventDefault();
		var get_id =$(this).find('a:first').attr('href');
		var ids = get_id.substring(1);
		var url = window.location.href; 
		var pathArray = window.location.href.split('?');
		lastpath= pathArray[0];
		lastpath += '?'+ids
		window.location.href = lastpath;
		// alert(lastpath);
	});


});

function dataTable(){
	
	$('#exportTable1').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print',

		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
						},
						
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
		
	});
	$('#exportTable2').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print',
			
		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
		
	});
	$('#exportTable3').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print'
		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
		
	});
	$('#exportTable4').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print'
		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
		
	});
	$('#exportTable5').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print'
		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
	});
	$('#exportTable6').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print'
		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
		
	});
	$('#exportTable7').DataTable( {
		dom: 'Bfrtip',
		// buttons: [
		// 	'copy', 'csv', 'excel', 'pdf', 'print'
		// ]
		buttons: [
       {
           extend: 'pdf',
					 footer: true,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
       },
       {
           extend: 'csv',
					 footer: false,
					 className: "tableedit",
           exportOptions: {
							columns: [0,1,2,3,4,5,6,7]
            }
          
       },
       {
           extend: 'excel',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'copy',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
			 },
			 {
           extend: 'print',
					 footer: false,
					 className: "tableedit",
					 exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            }
       }
			         
    ]  
		
	});
}



			
</script>
<script>
//for chnage the visibility of the other stream sub category name
	function hiddenRemove(){
		if($('#txtstream').val() =="Other")
			{
				$('#divother').removeAttr('hidden');
			}
			else{
				$('#divother').attr("hidden","true");
			}
	}
	//for passing the form value to save
	function save(){
		var subject1 = $('#txtsubname1').text();
				var subject2 = $('#txtsubname2').text();
				var subject3 = $('#txtsubname3').text();
				var subject4 = $('#txtsubname4').text();
				var subject5 = $('#txtsubname5').text();
				var result = "result";
				var data = $('form').serialize()+"&subject1="+subject1+"&subject2="+subject2+"&subject3="+subject3+"&subject4="+subject4+"&subject5="+subject5+"&result="+result;
				console.log(data);
		$.ajax({

			method:'POST',
			url:"load/save.php",
			data:data,
			dataType:"text",

		})
		.done(function (data) { 
			console.log(data);
			$.toast({
				heading: 'Result Successfully Added',
				text: 'Data Added Successfully.',
				position: 'top-right',
				loaderBg:'#ff6849',
				icon: 'success',
				hideAfter: 3500, 
				stack: 6
			});
			//clear all field
			$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
			
			$("#txtgrade").val($("#txtgrade option:first").val());
			$("#txtmedium").val($("#txtmedium option:first").val());
			$("#txtstream").val($("#txtstream option:first").val());
			$("#txtother").val($("#txtother option:first").val());
			$("#txtsitting").val($("#txtsitting option:first").val());
			hiddenRemove();
		})
		.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); 

		})

		

	}
	//for required validation
	function required(){
		
		$.toast({
			heading: 'Please Fill The All Details.',
			text: 'All fileds are must.',
			position: 'top-right',
			loaderBg:'#ff6849',
			icon: 'error',
			hideAfter: 3500
			
		});
	
	}


	$(document).ready(function(){
		$('input[type=text]').css('text-transform','capitalize'); // capitalize case

//for grade
$('#txtmedium').change(function(){

if(stream == "Other")
{
	
	var other = $('#txtother').val();
	$.ajax({ /* THEN THE AJAX CALL */
		type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
		url: "load/grade.php", /* PAGE WHERE WE WILL PASS THE DATA */
		data: {stream:stream,medium:medium,other:other}, /* THE DATA WE WILL BE PASSING */
		success: function(data){ /* GET THE TO BE RETURNED DATA */
			//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
		///	$('label[id*="txtsubname3"]').text('');
			$("#txtgrade").html(data); 
		console.log(data);
		}
	});

}
else
{
	//var other = $('#txtother').val();
	var other = 'not';
	$.ajax({ /* THEN THE AJAX CALL */
		type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
		url: "load/grade.php", /* PAGE WHERE WE WILL PASS THE DATA */
		data: {stream:stream,medium:medium,other:other}, /* THE DATA WE WILL BE PASSING */
		success: function(data){ /* GET THE TO BE RETURNED DATA */
			//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
		///	$('label[id*="txtsubname3"]').text('');
			$("#txtgrade").html(data); 
		console.log(data);
		}
	});

}


})
$('#txtstream').change(function(){
	hiddenRemove();
})
	

$( "form" ).on( "submit", function( event ) {
	event.preventDefault();
	//text box validation 
	
	if($('#txtstream').val() =="Other"){
			if($('#txtgrade').val()=="" ||$('#txtmedium').val()==""||$('#txtstream').val()==""||$('#txtother').val()==""||$('#txtfullname').val()==""||$('#txtindex').val()==""||$('#txtyear').val()==""||$('#txtsub1').val()==""||$('#txtsub2').val()==""||$('#txtsub3').val()==""||$('#txtenglish').val()==""||$('#txtgk').val()=="")
			{
				required();
				console.log("Select the value for all");
			}
			else
			{
				save();
		
			}
	}
	else
	{
		if($('#txtgrade').val()=="" ||$('#txtmedium').val()==""||$('#txtstream').val()==""||$('#txtfullname').val()==""||$('#txtindex').val()==""||$('#txtyear').val()==""||$('#txtsub1').val()==""||$('#txtsub2').val()==""||$('#txtsub3').val()==""||$('#txtenglish').val()==""||$('#txtgk').val()=="")
		{
			console.log("Select the value for all");
			required();
		}
		else
		{
			save();
		}
	}
	
});

	});
</script> 

<script>
	//for change the text of the subject lable
$('#txtstream').change(function(){

	var stream = $(this).val();
	if(stream!="Other")
	{ /* GET THE VALUE OF THE SELECTED DATA */
		var sub1 = "subject1";
		var sub2 = "subject2";
		var sub3 = "subject3";
		// var dataString = "stream="+stream+"sub1="+sub1; /* STORE THAT TO A DATA STRING */

		$.ajax({ /* THEN THE AJAX CALL */
			type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
			url: "load/dropdown.php", /* PAGE WHERE WE WILL PASS THE DATA */
			data: {stream:stream,sub1:sub1}, /* THE DATA WE WILL BE PASSING */
			success: function(data){ /* GET THE TO BE RETURNED DATA */
				//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
				$('label[id*="txtsubname1"]').text('');
				$("#txtsubname1").html(data+'<span class="required"> * </span>'); 
				console.log(data);
			}
		});
						
		$.ajax({ /* THEN THE AJAX CALL */
			type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
			url: "load/dropdownsub2.php", /* PAGE WHERE WE WILL PASS THE DATA */
			data: {stream:stream,sub2:sub2}, /* THE DATA WE WILL BE PASSING */
			success: function(data){ /* GET THE TO BE RETURNED DATA */
				//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
				$('label[id*="txtsubname2"]').text('');
				$("#txtsubname2").html(data+'<span class="required"> * </span>'); 
			console.log(data);
			}
		});
						
		$.ajax({ /* THEN THE AJAX CALL */
			type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
			url: "load/dropdownsub3.php", /* PAGE WHERE WE WILL PASS THE DATA */
			data: {stream:stream,sub3:sub3}, /* THE DATA WE WILL BE PASSING */
			success: function(data){ /* GET THE TO BE RETURNED DATA */
				//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
				$('label[id*="txtsubname3"]').text('');
				$("#txtsubname3").html(data+'<span class="required"> * </span>'); 
			console.log(data);
			}
		});
	}	//for change the text of the subject lable for other stream
	else if(stream=="Other")
	{
		$('#txtother').change(function(){
			var stream = $(this).val();
			var sub1 = "othersubject1";
			var sub2 = "othersubject2";
			var sub3 = "othersubject3";
			// var dataString = "stream="+stream+"sub1="+sub1; /* STORE THAT TO A DATA STRING */

			$.ajax({ /* THEN THE AJAX CALL */
				type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
				url: "load/dropdown.php", /* PAGE WHERE WE WILL PASS THE DATA */
				data: {stream:stream,sub1:sub1}, /* THE DATA WE WILL BE PASSING */
				success: function(data){ /* GET THE TO BE RETURNED DATA */
					//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
					$('label[id*="txtsubname1"]').text('');
					$("#txtsubname1").html(data+'<span class="required"> * </span>'); 
				console.log(data);
				}
			});
					
			$.ajax({ /* THEN THE AJAX CALL */
				type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
				url: "load/dropdownsub2.php", /* PAGE WHERE WE WILL PASS THE DATA */
				data: {stream:stream,sub2:sub2}, /* THE DATA WE WILL BE PASSING */
				success: function(data){ /* GET THE TO BE RETURNED DATA */
					//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
					$('label[id*="txtsubname2"]').text('');
					$("#txtsubname2").html(data+'<span class="required"> * </span>'); 
				console.log(data);
				}
			});
					
			$.ajax({ /* THEN THE AJAX CALL */
				type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
				url: "load/dropdownsub3.php", /* PAGE WHERE WE WILL PASS THE DATA */
				data: {stream:stream,sub3:sub3}, /* THE DATA WE WILL BE PASSING */
				success: function(data){ /* GET THE TO BE RETURNED DATA */
					//$("#txtsubname1").html(''); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
					$('label[id*="txtsubname3"]').text('');
						$("#txtsubname3").html(data+'<span class="required"> * </span>'); 
				console.log(data);
				}
			});
		})
	}

});


</script>

