<?php
include('header.php');
include('side-bar.php');
?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="width: 557px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">STUDENT DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <table border='1' class = table table-bordered>
                <thead>
                    <tr>
                        <td>Index No </td>
                        <td>Full Name </td>
                        <td>Division </td>

                    </tr>
                </thead>
                <tbody id='tbody'>
                    
                </tbody>


            
            </table>
         
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Result Details First Attempt </div>
					
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Result</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					
					
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Result Details</header>
						<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
							<i class="material-icons">more_vert</i>
						</button>
						<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
							<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
							<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
							<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
						</ul>
					</div>
					<div class="card-body" id="bar-parent">
                        <div class="row">
                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Bio Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count ,temp_results.index_no FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Bio' and temp_results.year='$date' and temp_results.sitting='First' ");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                       
                            echo '<tr>
                                <td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td>
                                
                                <td>'.$data1['count'] .'</td>
                                
                                <td><a href="#" class="btn btn-primary btn-xs bio" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td>
                            
                            </tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Maths Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Maths' and temp_results.year='$date' and temp_results.sitting='First' ");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                            echo '<tr><td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td><td>'.$data1['count'] .'</td><td><a href="#" class="btn btn-primary btn-xs Maths" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td></tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Commerce Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Commerce' and temp_results.year='$date' and temp_results.sitting='First'");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                            echo '<tr><td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td><td>'.$data1['count'] .'</td><td><a href="#" class="btn btn-primary btn-xs Commerce" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td></tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Arts Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Arts' and temp_results.year='$date'and temp_results.sitting='First' ");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                            echo '<tr><td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td><td>'.$data1['count'] .'</td><td><a href="#" class="btn btn-primary btn-xs Arts" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td></tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Eng Technology Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Eng-Technology' and temp_results.year='$date' and temp_results.sitting='First'");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                            echo '<tr><td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td><td>'.$data1['count'] .'</td><td><a href="#" class="btn btn-primary btn-xs etech" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td></tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Bio Technology Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Bio-Technology' and temp_results.year='$date' and temp_results.sitting='First'");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                            echo '<tr><td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td><td>'.$data1['count'] .'</td><td><a href="#" class="btn btn-primary btn-xs btech" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td></tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>

                    <div class="col-sm-4">
                    <div class="table-scrollable">
                    <?php 
                    include_once('load/connection.php');

                    //count all student
                    $output = '';
                    $date = date("Y");
                    echo '<table border=1 class = table table-bordered> <thead><tr><th  colspan=3 >Other Stream</th></tr><tr><th>Results</th><th>No of Students</th><th>View</th></tr></thead><tbody>';
                    $sql = $mysqli->query("select * FROM results_group");

                    while ($data = $sql->fetch_array() ) {
                    
                        $sql1 = $mysqli->query("select count(temp_results.index_no) as count FROM temp_results left join results_group on temp_results.id = results_group.id where temp_results.a='$data[a]' and temp_results.b='$data[b]' and temp_results.c='$data[c]' and temp_results.s='$data[s]' and temp_results.f='$data[f]' and temp_results.stream='Other' and temp_results.year='$date' and temp_results.sitting='First'");

                        

                        while ($data1 = $sql1->fetch_array() ) {
                            echo '<tr><td>'.($data['a']!=0?($data['a']!=1?$data['a'].' A ':' A '):'').($data['b']!=0?($data['b']!=1?$data['b'].' B ':' B '):'').($data['c']!=0?($data['c']!=1?$data['c'].' C ':' C '):'').($data['s']!=0?($data['s']!=1?$data['s'].' S ':' S '):'').($data['f']!=0?($data['f']!=1?$data['f'].' F ':' F ' ):'').' </td><td>'.$data1['count'] .'</td><td><a href="#" class="btn btn-primary btn-xs other" data-toggle=modal data-target=#exampleModalCenter id='.$data['a'].$data['b'].$data['c'].$data['s'].$data['f'] .' ><i class="fa fa-eye"></i></a></td></tr>';
                        } 

                       
                    }

                    echo '</tbody><tfoot></tfoot></table> ';

                        ?>
                    </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
			</div>
            </div>

            </div>

<?php

include('footer.php');

?>

<script>
$(document).ready(function(){

    $('.bio').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='Bio-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
        
                 $("#tbody").html(data); 
                
            }
        })
    })

    $('.Maths').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='Maths-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
             
                 $("#tbody").html(data); 
                
            }
        })
    })

    $('.Commerce').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='Commerce-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
             
                 $("#tbody").html(data); 
                
            }
        })
    })

    $('.Arts').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='Arts-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
             
                 $("#tbody").html(data); 
                
            }
        })
    })

    $('.etech').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='etech-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
             
                 $("#tbody").html(data); 
                
            }
        })
    })

    $('.btech').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='btech-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
             
                 $("#tbody").html(data); 
                
            }
        })
    })

    $('.other').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var stream ='other-first';
       // console.log(id);
        $.ajax({
            type: "POST",
            url: "results-report-query.php", 
            data: {id:id,stream:stream},
            dataType:"text",  
            success: function(data){ 
             
                 $("#tbody").html(data); 
                
            }
        })
    })


});

</script>
