<?php
    include('header.php');
    include('side-bar.php');
?>
<?php
 
 $dataPoints = array();
 $dataPoints2 = array();
 $dataPoints3 = array();
 $dataPoints4 = array();
 $dataPoints5 = array();
 $dataPoints6 = array();
 $dataPoints7 = array();
 try{
    
     $link = new \PDO(   'mysql:host=localhost;dbname=rms_db;charset=utf8mb4', //'mysql:host=localhost;dbname=canvasjs_db;charset=utf8mb4',
                         'root', //'root',
                         '', //'',
                         array(
                             \PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                             \PDO::ATTR_PERSISTENT => false
                         )
                     );
    $date = date("Y");
    // for bio
     $handle = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Bio' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle->execute(); 
     $result = $handle->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result as $row){
        
        
         array_push($dataPoints, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
//=======================================================
// for maths
     $handle2 = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Maths' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle2->execute(); 
     $result2 = $handle2->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result2 as $row){
        
        
         array_push($dataPoints2, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
     //=================================================
     //for commerce
     $handle3 = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Commerce' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle3->execute(); 
     $result3 = $handle3->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result3 as $row){
        
        
         array_push($dataPoints3, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
    
     //=================================================
     //for arts
     $handle4 = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Arts' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle4->execute(); 
     $result4 = $handle4->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result4 as $row){
        
        
         array_push($dataPoints4, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
    
     //=================================================
     //for bio tech
     $handle5 = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Bio-Technology' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle5->execute(); 
     $result5 = $handle5->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result5 as $row){
        
        
         array_push($dataPoints5, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
    //=================================================
     //for eng tech
     $handle6 = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Eng-Technology' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle6->execute(); 
     $result6 = $handle6->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result6 as $row){
        
        
         array_push($dataPoints6, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
    //=================================================
     //for other
     $handle7 = $link->prepare("SELECT count(results.result) as total ,results.result FROM `results`inner join students on results.index_no = students.index_no WHERE results.stream='Other' and results.subject_id!='General English' and results.subject_id!='GK' and students.year='$date' group by results.result"); 
    
     $handle7->execute(); 
     $result7 = $handle7->fetchAll(\PDO::FETCH_OBJ);
   
 
     foreach($result7 as $row){
        
        
         array_push($dataPoints7, array("label"=> $row->result, "y"=> $row->total));
         //print_r($result);
     }
    
     $link = null;
 }
 catch(\PDOException $ex){
     print($ex->getMessage());
 }
     
 ?>
 
    


 <div class="page-content-wrapper">
				<div class="page-content" style="min-height: 558px;">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Stream Result Information</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Report</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Stream Result</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Stream Vice Information</header>
									<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
										<i class="material-icons">more_vert</i>
									</button>
									<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="panel-button">
										<li class="mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
										<li class="mdl-menu__item"><i class="material-icons">print</i>Another action</li>
										<li class="mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
									</ul>
                                </div>
                                
								<div class="card-body" id="bar-parent">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="chartContainer" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="chartContainer2" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="chartContainer3" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="chartContainer4" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="chartContainer5" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="chartContainer6" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="chartContainer7" style="height: 370px; width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
            </div>



 </body>
 </html>  
 <?php
 include('footer.php');
 ?>
  <script>
 
 $(document).ready(function(e){
  
 var chart = new CanvasJS.Chart("chartContainer", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Bio Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
        
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })

 var chart2 = new CanvasJS.Chart("chartContainer2", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Maths Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })

 var chart3 = new CanvasJS.Chart("chartContainer3", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Commerce Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })
 var chart4 = new CanvasJS.Chart("chartContainer4", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Arts Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints4, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })
 var chart5 = new CanvasJS.Chart("chartContainer5", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Bio Technology Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints5, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })
 var chart6 = new CanvasJS.Chart("chartContainer6", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Engineering Technology Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints6, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })
 var chart7 = new CanvasJS.Chart("chartContainer7", {
     animationEnabled: true,
     exportEnabled: true,
     theme: "light2", // "light1", "light2", "dark1", "dark2"
     title:{
        text: "Other Stream"              
    },
    ticks: {
        autoSkip: true,
    
    },
    axisX:{
        tickColor: "red",
        tickLength: 5,
        tickThickness: 2,
        title:"Grade",
        skipDataMain:2,
        label:" ",
    
    },
    axisY:{
        tickLength: 5,
        tickColor: "DarkSlateBlue" ,
        tickThickness: 2,
        title:"No of Students",
        skipDataMain:2,
        label:" ",
        
    
    },
    dataPointWidth: 20,
    
    data: [           
        { 
    
            color: "#4661EE",
            type: "column",
            dataPoints: <?php echo json_encode($dataPoints7, JSON_NUMERIC_CHECK); ?>,
        }
    ]
   
 })
 chart.render();
 chart2.render();
 chart3.render();
 chart4.render();
 chart5.render();
 chart6.render();
 chart7.render();

});
 
 </script>