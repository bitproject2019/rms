<?php
include('header.php');
include('side-bar.php');

?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Dashboard</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
        <div class="state-overview">
            <div class="row">
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-pink">
                        <span class="info-box-icon push-bottom"><i class="material-icons">group</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Students</span>
                            <span class="info-box-number" id="total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="total-progress" ></div>
                            </div>
                            <span class="progress-description">
                             100%
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bio </span>
                            <span class="info-box-number" id="bio-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="bio-progress"></div>
                            </div>
                            <span class="progress-description" id="bio-progrss-description">
                                
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Maths </span>
                            <span class="info-box-number" id="maths-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="maths-progress"></div>
                            </div>
                            <span class="progress-description" id="maths-progrss-description">
                                
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Commerce </span>
                            <span class="info-box-number" id="commerce-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="commerce-progress"></div>
                            </div>
                            <span class="progress-description" id="commerce-progrss-description">
                                
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Arts </span>
                            <span class="info-box-number" id="arts-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="arts-progress"></div>
                            </div>
                            <span class="progress-description" id="arts-progrss-description">
                                
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Eng Tech </span>
                            <span class="info-box-number" id="engineering-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="engineering-progress"></div>
                            </div>
                            <span class="progress-description" id="engineering-progrss-description">
                                
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bio Tech</span>
                            <span class="info-box-number" id="biotech-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="biotech-progress"></div>
                            </div>
                            <span class="progress-description" id="biotech-progrss-description">
                              
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <div class="col-xl-3 col-md-6 col-12">
                    <div class="info-box bg-b-blue">
                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Other Stream</span>
                            <span class="info-box-number" id="other-total"></span>
                            <div class="progress">
                                <div class="progress-bar" id="other-progress"></div>
                            </div>
                            <span class="progress-description" id="other-progrss-description">
                              
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>
</div>
</div>
<?php
include('footer.php');
?>

<script>
$(document).ready(function(){

   // var result = "totalstudent";
    $.ajax({
        method:'POST',
        url:"load/report.php",
        
        dataType:"json",
        success:function(res){
            //console.log(res.tot);
            //for total students
            $('#total').html(res.tot);
            $('#total-progress').css("width","100%");
            //for bio students
            $('#bio-total').html(res.bio);
            $('#bio-progress').css("width",res.percent);
            $('#bio-progrss-description').html(res.percent+"% in total");

            //for maths students
            $('#maths-total').html(res.maths);
            $('#maths-progress').css("width",res.mathspercent);
            $('#maths-progrss-description').html(res.mathspercent+"% in total");

            //for commerce students
            $('#commerce-total').html(res.commerce);
            $('#commerce-progress').css("width",res.commercepercent);
            $('#commerce-progrss-description').html(res.commercepercent+"% in total");

            //for arts students
            $('#arts-total').html(res.arts);
            $('#arts-progress').css("width",res.artspercent);
            $('#arts-progrss-description').html(res.artspercent+"% in total");
            
            //for engineering students
            $('#engineering-total').html(res.engineering);
            $('#engineering-progress').css("width",res.engineeringpercent);
            $('#engineering-progrss-description').html(res.engineeringpercent+"% in total");
            
            //for biotech students
            $('#biotech-total').html(res.biotech);
            $('#biotech-progress').css("width",res.biotechpercent);
            $('#biotech-progrss-description').html(res.biotechpercent+"% in total");
            
            //for other students
            $('#other-total').html(res.other);
            $('#other-progress').css("width",res.otherpercent);
            $('#other-progrss-description').html(res.otherpercent+"% in total");

        }
    })

   
});
</script>